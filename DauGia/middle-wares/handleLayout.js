var q = require('q'),
    danhmucRepo = require('../models/danhmucRepo');

module.exports = function(req, res, next) {
    q.all([
    	danhmucRepo.loadAllDm(),
    	danhmucRepo.loadAllDmc()
    ]).spread(function(cRows,dRows) {
		res.locals.layoutVM = {
			danhmuc: cRows,
			danhmuccon: dRows,
			flag1: true,
			flag2: cRows[0].iddanhmuc===dRows[1].danhmuc,
			flag3: cRows[0].iddanhmuc===dRows[2].danhmuc,
			flag4: cRows[0].iddanhmuc===dRows[3].danhmuc,
			flag5: cRows[1].iddanhmuc===dRows[4].danhmuc,
			flag6: cRows[1].iddanhmuc===dRows[5].danhmuc,
			// suppliers: []
		};
    	next();
    });
	
}