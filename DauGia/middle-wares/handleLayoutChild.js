var q = require('q'),
    danhmucconRepo = require('../models/danhmucconRepo');

module.exports = function(req, res, next) {
    q.all([
    	danhmucconRepo.loadAllDmc()
    ]).spread(function(cRows) {
		res.locals.layoutVM = {
			danhmuccon: cRows,
			// suppliers: []
		};
    		next();
    });
	
}