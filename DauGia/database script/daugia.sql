/*
Navicat MySQL Data Transfer

Source Server         : sqlsql
Source Server Version : 50714
Source Host           : 127.0.0.1:3306
Source Database       : daugia

Target Server Type    : MYSQL
Target Server Version : 50714
File Encoding         : 65001

Date: 2017-06-30 10:07:23
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for bid_log
-- ----------------------------
DROP TABLE IF EXISTS `bid_log`;
CREATE TABLE `bid_log` (
  `idbid_log` int(11) NOT NULL AUTO_INCREMENT,
  `idsanpham` int(11) DEFAULT NULL,
  `nguoimua` int(11) DEFAULT NULL,
  `gia` int(11) DEFAULT NULL,
  `thoigian` datetime DEFAULT NULL,
  PRIMARY KEY (`idbid_log`)
) ENGINE=MyISAM AUTO_INCREMENT=83 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of bid_log
-- ----------------------------
INSERT INTO `bid_log` VALUES ('1', '1', '1', '8800000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('2', '1', '2', '9000000', '2017-06-23 12:30:00');
INSERT INTO `bid_log` VALUES ('3', '5', '3', '4200000', '2017-06-23 12:20:00');
INSERT INTO `bid_log` VALUES ('4', '5', '4', '4500000', '2017-06-23 13:00:00');
INSERT INTO `bid_log` VALUES ('5', '2', '2', '10200000', '2017-06-23 12:50:00');
INSERT INTO `bid_log` VALUES ('6', '3', '1', '7200000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('7', '3', '5', '7500000', '2017-06-23 12:15:00');
INSERT INTO `bid_log` VALUES ('8', '3', '6', '7800000', '2017-06-23 12:30:00');
INSERT INTO `bid_log` VALUES ('9', '4', '4', '5200000', '2017-06-23 12:20:00');
INSERT INTO `bid_log` VALUES ('10', '4', '3', '5400000', '2017-06-23 12:30:00');
INSERT INTO `bid_log` VALUES ('11', '9', '6', '17500000', '2017-06-23 12:30:00');
INSERT INTO `bid_log` VALUES ('12', '9', '4', '18000000', '2017-06-23 12:50:00');
INSERT INTO `bid_log` VALUES ('13', '6', '3', '4700000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('14', '7', '5', '5200000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('15', '8', '1', '9200000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('16', '10', '3', '180000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('17', '11', '3', '80000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('18', '12', '4', '100000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('19', '13', '2', '420000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('20', '13', '5', '440000', '2017-06-23 12:50:00');
INSERT INTO `bid_log` VALUES ('21', '14', '5', '320000', '2017-06-23 12:20:00');
INSERT INTO `bid_log` VALUES ('22', '14', '6', '340000', '2017-06-23 12:40:00');
INSERT INTO `bid_log` VALUES ('23', '15', '1', '650000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('24', '16', '3', '1100000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('25', '17', '4', '1100000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('26', '18', '3', '3700000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('27', '19', '5', '650000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('28', '20', '6', '6700000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('29', '21', '2', '300000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('30', '22', '1', '650000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('31', '23', '2', '90000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('32', '23', '5', '100000', '2017-06-23 12:20:00');
INSERT INTO `bid_log` VALUES ('33', '24', '6', '900000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('34', '25', '3', '110000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('35', '25', '4', '120000', '2017-06-23 12:20:00');
INSERT INTO `bid_log` VALUES ('36', '26', '2', '170000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('37', '26', '1', '180000', '2017-06-23 12:30:00');
INSERT INTO `bid_log` VALUES ('38', '27', '3', '450000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('39', '28', '5', '60000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('40', '29', '3', '130000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('41', '29', '2', '140000', '2017-06-23 12:30:00');
INSERT INTO `bid_log` VALUES ('42', '30', '6', '50000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('43', '31', '6', '100000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('44', '32', '4', '80000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('45', '33', '5', '110000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('46', '33', '4', '120000', '2017-06-23 12:30:00');
INSERT INTO `bid_log` VALUES ('47', '34', '3', '90000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('48', '34', '2', '100000', '2017-06-23 12:50:00');
INSERT INTO `bid_log` VALUES ('49', '35', '2', '70000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('50', '36', '6', '30000', '2017-06-23 12:20:00');
INSERT INTO `bid_log` VALUES ('51', '37', '1', '170000', '2017-06-23 12:40:00');
INSERT INTO `bid_log` VALUES ('52', '38', '2', '60000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('53', '38', '1', '70000', '2017-06-23 12:30:00');
INSERT INTO `bid_log` VALUES ('54', '39', '1', '110000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('55', '40', '3', '90000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('56', '40', '6', '100000', '2017-06-23 12:20:00');
INSERT INTO `bid_log` VALUES ('57', '41', '1', '70000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('58', '42', '2', '30000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('59', '43', '3', '620000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('60', '43', '4', '640000', '2017-06-23 12:50:00');
INSERT INTO `bid_log` VALUES ('61', '44', '4', '100000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('62', '45', '2', '220000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('63', '46', '3', '270000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('64', '47', '3', '60000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('65', '48', '5', '340000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('66', '49', '6', '180000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('67', '50', '1', '240000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('68', '51', '4', '270000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('69', '52', '2', '320000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('70', '52', '3', '340000', '2017-06-23 12:30:00');
INSERT INTO `bid_log` VALUES ('71', '53', '3', '25000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('72', '53', '4', '30000', '2017-06-23 12:40:00');
INSERT INTO `bid_log` VALUES ('73', '54', '4', '35000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('74', '54', '5', '40000', '2017-06-23 12:50:00');
INSERT INTO `bid_log` VALUES ('75', '55', '1', '90000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('76', '56', '4', '60000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('77', '57', '3', '110000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('78', '58', '2', '70000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('79', '59', '6', '1700000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('80', '60', '5', '170000', '2017-06-23 12:10:00');
INSERT INTO `bid_log` VALUES ('81', '1', '1', '9100000', '2017-06-30 00:04:03');
INSERT INTO `bid_log` VALUES ('82', '1', '2', '9200000', '2017-06-30 08:56:55');

-- ----------------------------
-- Table structure for chitietdanhgia
-- ----------------------------
DROP TABLE IF EXISTS `chitietdanhgia`;
CREATE TABLE `chitietdanhgia` (
  `userID` int(11) NOT NULL,
  `reviewID` int(11) NOT NULL,
  `content` varchar(255) NOT NULL,
  `cong` int(11) DEFAULT NULL,
  `tru` int(11) DEFAULT NULL,
  `nguoidanhgiaID` int(11) NOT NULL,
  `timestamp` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`reviewID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of chitietdanhgia
-- ----------------------------
INSERT INTO `chitietdanhgia` VALUES ('1', '1', 'abc', '1', '2', '2', '2017-06-30 01:02:59');
INSERT INTO `chitietdanhgia` VALUES ('1', '2', 'abcd', '0', '1', '1', '2017-06-30 01:04:23');
INSERT INTO `chitietdanhgia` VALUES ('2', '3', 'aa', '0', '0', '3', '2017-06-30 02:22:52');
INSERT INTO `chitietdanhgia` VALUES ('3', '4', 'a', '0', '0', '4', '2017-06-30 02:24:15');

-- ----------------------------
-- Table structure for danhmuc
-- ----------------------------
DROP TABLE IF EXISTS `danhmuc`;
CREATE TABLE `danhmuc` (
  `iddanhmuc` int(11) NOT NULL AUTO_INCREMENT,
  `namedanhmuc` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`iddanhmuc`)
) ENGINE=MyISAM AUTO_INCREMENT=9 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of danhmuc
-- ----------------------------
INSERT INTO `danhmuc` VALUES ('1', 'Điện tử');
INSERT INTO `danhmuc` VALUES ('2', 'Nhạc cụ');
INSERT INTO `danhmuc` VALUES ('3', 'Thời trang');
INSERT INTO `danhmuc` VALUES ('4', 'Sức khỏe & Sắc đẹp');
INSERT INTO `danhmuc` VALUES ('5', 'Trẻ em & Đồ chơi');
INSERT INTO `danhmuc` VALUES ('6', 'Sách');
INSERT INTO `danhmuc` VALUES ('7', 'Đồ gia dụng');
INSERT INTO `danhmuc` VALUES ('8', 'Khác');

-- ----------------------------
-- Table structure for danhmuccon
-- ----------------------------
DROP TABLE IF EXISTS `danhmuccon`;
CREATE TABLE `danhmuccon` (
  `iddanhmuccon` int(11) NOT NULL AUTO_INCREMENT,
  `namedanhmuccon` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `danhmuc` int(11) NOT NULL,
  PRIMARY KEY (`iddanhmuccon`)
) ENGINE=MyISAM AUTO_INCREMENT=16 DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of danhmuccon
-- ----------------------------
INSERT INTO `danhmuccon` VALUES ('1', 'Điện thoại & Máy tính bảng', '1');
INSERT INTO `danhmuccon` VALUES ('2', 'Máy tính & Laptop', '1');
INSERT INTO `danhmuccon` VALUES ('3', 'Máy ảnh & Máy quay phim', '1');
INSERT INTO `danhmuccon` VALUES ('4', 'TV, Video, Âm thanh, Game & Thiết bị số', '1');
INSERT INTO `danhmuccon` VALUES ('5', 'Ghi-ta', '2');
INSERT INTO `danhmuccon` VALUES ('6', 'Ukulele', '2');
INSERT INTO `danhmuccon` VALUES ('7', 'Bộ gõ & Trống', '2');
INSERT INTO `danhmuccon` VALUES ('8', 'Thời trang nam', '3');
INSERT INTO `danhmuccon` VALUES ('9', 'Thời trang nữ', '3');
INSERT INTO `danhmuccon` VALUES ('10', 'Phụ kiện', '3');
INSERT INTO `danhmuccon` VALUES ('11', 'Trang điểm', '4');
INSERT INTO `danhmuccon` VALUES ('12', 'Thực phẩm chức năng', '4');
INSERT INTO `danhmuccon` VALUES ('13', 'Đồ chơi lego', '5');
INSERT INTO `danhmuccon` VALUES ('14', 'Đồ chơi gỗ', '5');
INSERT INTO `danhmuccon` VALUES ('15', 'Búp bê & Gấu bông', '5');

-- ----------------------------
-- Table structure for doiban
-- ----------------------------
DROP TABLE IF EXISTS `doiban`;
CREATE TABLE `doiban` (
  `userID` int(11) NOT NULL,
  `time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  KEY `userID` (`userID`),
  CONSTRAINT `doiban_ibfk_1` FOREIGN KEY (`userID`) REFERENCES `users` (`userID`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of doiban
-- ----------------------------
INSERT INTO `doiban` VALUES ('2', '2017-06-28 21:53:42');
INSERT INTO `doiban` VALUES ('4', '2017-06-27 21:53:47');
INSERT INTO `doiban` VALUES ('6', '2017-06-19 21:53:51');
INSERT INTO `doiban` VALUES ('7', '2017-06-28 23:50:04');

-- ----------------------------
-- Table structure for nguoiban
-- ----------------------------
DROP TABLE IF EXISTS `nguoiban`;
CREATE TABLE `nguoiban` (
  `idnguoiban` int(11) NOT NULL,
  `namenguoiban` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `diemdanhgia` int(11) DEFAULT NULL,
  PRIMARY KEY (`idnguoiban`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of nguoiban
-- ----------------------------
INSERT INTO `nguoiban` VALUES ('1', 'Seller1', '0');
INSERT INTO `nguoiban` VALUES ('2', 'Seller2', '0');
INSERT INTO `nguoiban` VALUES ('3', 'Seller3', '0');
INSERT INTO `nguoiban` VALUES ('4', 'Seller4', '0');
INSERT INTO `nguoiban` VALUES ('5', 'Seller5', '0');
INSERT INTO `nguoiban` VALUES ('6', 'Seller6', '0');
INSERT INTO `nguoiban` VALUES ('7', 'Seller7', '0');
INSERT INTO `nguoiban` VALUES ('8', 'Seller8', '0');

-- ----------------------------
-- Table structure for nguoimua
-- ----------------------------
DROP TABLE IF EXISTS `nguoimua`;
CREATE TABLE `nguoimua` (
  `idnguoimua` int(11) NOT NULL,
  `namenguoimua` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  PRIMARY KEY (`idnguoimua`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of nguoimua
-- ----------------------------
INSERT INTO `nguoimua` VALUES ('1', 'Red');
INSERT INTO `nguoimua` VALUES ('2', 'Natsudragoneel');
INSERT INTO `nguoimua` VALUES ('3', 'Yellow');
INSERT INTO `nguoimua` VALUES ('4', 'Blue');
INSERT INTO `nguoimua` VALUES ('5', 'White');
INSERT INTO `nguoimua` VALUES ('6', 'Black');
INSERT INTO `nguoimua` VALUES ('7', 'hieuphan');

-- ----------------------------
-- Table structure for sanpham
-- ----------------------------
DROP TABLE IF EXISTS `sanpham`;
CREATE TABLE `sanpham` (
  `idsanpham` int(11) NOT NULL,
  `name` varchar(45) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price` int(11) DEFAULT NULL,
  `Bid` int(11) DEFAULT NULL,
  `timestart` datetime DEFAULT NULL,
  `timeend` datetime DEFAULT NULL,
  `danhmuccon` int(11) DEFAULT NULL,
  `intro` text COLLATE utf8_unicode_ci,
  `receiveinfo` text COLLATE utf8_unicode_ci,
  `warranty` text COLLATE utf8_unicode_ci,
  `danhmuc` int(11) DEFAULT NULL,
  `giakhoidiem` int(11) DEFAULT NULL,
  `idhinh` int(11) DEFAULT NULL,
  `nguoiban` int(11) DEFAULT NULL,
  PRIMARY KEY (`idsanpham`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- ----------------------------
-- Records of sanpham
-- ----------------------------
INSERT INTO `sanpham` VALUES ('1', 'Samsung Galaxy Note 7', '11000000', '4', '2017-06-23 12:00:00', '2017-06-30 13:15:00', '1', '<ul>\n<li>Màn hình: Super AMOLED, 5.7\", Quad HD (2K)</li>\n<li>Hệ điều hành: Android 6.0 (Marshmallow)</li>\n<li>Camera sau: 12 MP</li>\n<li>Camera trước: 5 MP</li>\n<li>CPU: Exynos 8890 8 nhân 64-bit</li>\n<li>RAM: 4 GB</li>\n<li>Bộ nhớ trong: 64 GB</li>\n<li>Thẻ SIM: 1 Nano SIM</li>\n<li>Dung lượng pin: 3200 mAh</li>\n</ul>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '8000000', '3', '1');
INSERT INTO `sanpham` VALUES ('2', 'Samsung Galaxy Tab S3', '16000000', '1', '2017-06-23 12:00:00', '2017-07-05 12:00:00', '1', '<UL>\n<LI>Màn hình: Super AMOLED, 9.7\"</LI>\n<LI>Hệ điều hành: Android 7.0</LI>\n<LI>CPU: Qualcomm MSM 8996, 2 nhân 2.15 GHz & 2 nhân 1.6 GHz</LI>\n<LI>RAM: 4 GB</LI>\n<LI>Bộ nhớ trong: 32 GB</LI>\n<LI>Camera sau: 13 MP</LI>\n<LI>Camera trước: 5 MP</LI>\n<LI>Kết nối mạng: WiFi, 3G, 4G LTE</LI>\n<LI>Hỗ trợ SIM: Micro sim</LI>\n<LI>Đàm thoại: Có</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.', '1', '10000000', '5', '2');
INSERT INTO `sanpham` VALUES ('3', 'Sony Xperia Z5 Dual ', '10900000', '3', '2017-06-23 12:00:00', '2017-06-30 12:00:00', '1', '<UL>\n<LI>Màn hình: TRILUMINOS™, 5.2\", Full HD</LI>\n<LI>Hệ điều hành: Android 7.0</LI>\n<LI>Camera sau: 23 MP</LI>\n<LI>Camera trước: 13 MP</LI>\n<LI>CPU: Snapdragon 820 4 nhân 64-bit</LI>\n<LI>RAM: 3 GB</LI>\n<LI>Bộ nhớ trong: 64 GB</LI>\n<LI>Thẻ nhớ: MicroSD, hỗ trợ tối đa 256 GB</LI>\n<LI>Thẻ SIM: 2 SIM Nano (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G</LI>\n<LI>Dung lượng pin: 2900 mAh, có sạc nhanh</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '7000000', '2', '3');
INSERT INTO `sanpham` VALUES ('4', 'Oppo F3', '7000000', '2', '2017-06-23 12:00:00', '2017-07-02 12:00:00', '1', '<UL>\n<LI>Màn hình: IPS LCD, 5.5\", Full HD</LI>\n<LI>Hệ điều hành: Android 6.0 (Marshmallow)</LI>\n<LI>Camera sau:	13 MP</LI>\n<LI>Camera trước: 16 MP và 8 MP</LI>\n<LI>CPU: MT6750T 8 nhân 64-bit</LI>\n<LI>RAM: 4 GB</LI>\n<LI>Bộ nhớ trong: 64 GB</LI>\n<LI>Thẻ nhớ: MicroSD, hỗ trợ tối đa 128 GB</LI>\n<LI>Thẻ SIM: 2 Nano SIM, Hỗ trợ 4G</LI>\n<LI>Dung lượng pin: 3200 mAh</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '5000000', '1', '1');
INSERT INTO `sanpham` VALUES ('5', 'Asus Zenfone 3 Ze520KL', '6000000', '2', '2017-06-23 12:00:00', '2017-06-28 12:00:00', '1', '<UL>\n<LI>Màn hình:	Super IPS LCD, 5.2\", Full HD</LI>\n<LI>Hệ điều hành:	Android 6.0 (Marshmallow)</LI>\n<LI>Camera sau:	16 MP</LI>\n<LI>Camera trước:	8 MP</LI>\n<LI>CPU:	Snapdragon 625 8 nhân 64-bit</LI>\n<LI>RAM:	4 GB</LI>\n<LI>Bộ nhớ trong:	64 GB</LI>\n<LI>Thẻ nhớ:	MicroSD, hỗ trợ tối đa 128 GB</LI>\n<LI>Thẻ SIM:	Nano SIM & Micro SIM (SIM 2 chung khe thẻ nhớ), Hỗ trợ 4G</LI>\n<LI>Dung lượng pin:	2650 mAh</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '4000000', '4', '5');
INSERT INTO `sanpham` VALUES ('6', 'Asus E402NA N3350', '6500000', '1', '2017-06-23 12:00:00', '2017-06-29 12:00:00', '2', '<UL>\n<LI>CPU:	Intel Celeron, N3350, 1.10 GHz</LI>\n<LI>RAM:	2 GB, DDR3L(On board), 1600 MHz</LI>\n<LI>Ổ cứng:	HDD: 500 GB</LI>\n<LI>Màn hình:	14 inch, HD (1366 x 768)</LI>\n<LI>Card màn hình:	Card đồ họa tích hợp, Intel® HD Graphics</LI>\n<LI>Cổng kết nối:	HDMI, LAN (RJ45), USB 2.0, USB 3.0</LI>\n<LI>Hệ điều hành:	Windows 10</LI>\n<LI>Thiết kế:	Vỏ nhựa, PIN liền</LI>\n<LI>Kích thước:	21.9 mm, 1.65 kg</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '4500000', '4', '1');
INSERT INTO `sanpham` VALUES ('7', 'Acer ES1 533 N4200', '7000000', '1', '2017-06-23 12:00:00', '2017-07-03 12:00:00', '2', '<UL>\n<LI>CPU:	Intel Pentium, N4200, 1.10 GHz</LI>\n<LI>RAM:	4 GB, DDR3L (1 khe RAM), 1600 MHz</LI>\n<LI>Ổ cứng:	HDD: 500 GB</LI>\n<LI>Màn hình:	15.6 inch, HD (1366 x 768)</LI>\n<LI>Card màn hình:	Card đồ họa tích hợp, Intel® HD Graphics</LI>\n<LI>Cổng kết nối:	2 x USB 2.0, HDMI, LAN (RJ45), USB 3.0</LI>\n<LI>Hệ điều hành:	Windows 10</LI>\n<LI>Thiết kế:	Vỏ nhựa, PIN liền</LI>\n<LI>Kích thước:	Dày 24.6 mm, 2.4 kg</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '5000000', '1', '1');
INSERT INTO `sanpham` VALUES ('8', 'Dell Inspiron 7460 i5', '11000000', '1', '2017-06-23 12:00:00', '2017-06-29 12:00:00', '2', '<UL>\n<LI>CPU:	Intel Core i3 Kabylake, 7100U, 2.30 GHz</LI>\n<LI>RAM:	4 GB, DDR4 (1 khe), 2400 MHz</LI>\n<LI>Ổ cứng:	HDD: 1 TB</LI>\n</UL>\n<LI>Màn hình:	14 inch, HD (1366 x 768)</LI>\n<LI>Card màn hình:	Card đồ họa tích hợp, Intel® HD Graphics 620</LI>\n<LI>Cổng kết nối:	2 x USB 3.0, HDMI, LAN (RJ45), USB 2.0</LI>\n<LI>Hệ điều hành:	Windows 10</LI>\n<LI>Thiết kế:	Vỏ nhựa, PIN rời</LI>\n<LI>Kích thước:	Dày 21.4 mm, 1.77 kg</LI>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '9000000', '3', '1');
INSERT INTO `sanpham` VALUES ('9', 'Apple Macbook 12\"', '21500000', '2', '2017-06-23 12:00:00', '2017-06-30 12:00:00', '2', '<UL>\n<LI>CPU:	Intel Core i3 Kabylake, 7100U, 2.40 GHz</LI>\n<LI>RAM:	4 GB, DDR4 (1 khe), 2400 MHz</LI>\n<LI>Ổ cứng:	HDD: 1 TB</LI>\n<LI>Màn hình:	14 inch, HD (1366 x 768)</LI>\n<LI>Card màn hình:	Card đồ họa tích hợp, Intel® HD Graphics 620</LI>\n<LI>Cổng kết nối:	2 x USB 3.0, HDMI, LAN (RJ45), USB 2.0, VGA (D-Sub)</LI>\n<LI>Hệ điều hành:	Windows 10</LI>\n<LI>Thiết kế:	Vỏ nhựa, PIN rời</LI>\n<LI>Kích thước:	Dày 23.35 mm, 1.96</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '17000000', '2', '1');
INSERT INTO `sanpham` VALUES ('10', 'SanDisk Ultra 16GB', '170000', '1', '2017-06-23 12:00:00', '2017-06-25 12:00:00', '4', '<UL>\n<LI>Loại thẻ:	Thẻ Micro SD</LI>\n<LI>Dung lượng:	16 GB</LI>\n<LI>Tốc độ đọc:	30 MB/s</LI>\n<LI>Tốc độ ghi:	10 MB/s</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '100000', '2', '1');
INSERT INTO `sanpham` VALUES ('11', 'Chuột Có Dây Genius NS200 ', '150000', '1', '2017-06-23 12:00:00', '2017-07-09 12:00:00', '4', '<UL>\n<LI>Nhà sản xuất:	Genius</LI>\n<LI>Model:	DX-125</LI>\n<LI>Độ phân giải quang học:	1200 dpi</LI>\n<LI>Cách kết nối:	Cổng USB</LI>\n<LI>Độ dài dây / Khoảng cách kết nối:	Dây dài 150 cm</LI>\n<LI>Kích thước:	Dài 10.5 cm - ngang 6 cm - cao 3.7 cm</LI>\n<LI>Trọng lượng: 85 g</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '60000', '3', '1');
INSERT INTO `sanpham` VALUES ('12', 'USB Transcend JF350 2.0 8GB', '140000', '1', '2017-06-23 12:00:00', '2017-07-02 12:00:00', '4', '<UL>\n<LI>Nhà sản xuất:	Transcend</LI>\n<LI>Model:	JetFlash 360</LI>\n<LI>Dung lượng:	8 GB</LI>\n<LI>Loại USB:	USB 2.0</LI>\n<LI>Tốc độ đọc:	20 MB/s</LI>\n<LI>Tốc độ ghi:	5 MB/s</LI>\n<LI>Kích thước:	Dài 4.5 cm - ngang 1.8 cm - dày 0.7 cm</LI>\n<LI>Trọng lượng:	4.3 g</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '90000', '4', '1');
INSERT INTO `sanpham` VALUES ('13', 'MicroSDXC Kingston U1 80MB/s 64GB', '600000', '2', '2017-06-23 12:00:00', '2017-06-30 12:00:00', '4', '<UL>\n<LI>Loại thẻ:	</LI>\n<LI>Thẻ Micro SD</LI>\n<LI>Dung lượng:	64 GB</LI>\n<LI>Tốc độ đọc:	45 MB/s</LI>\n<LI>Tốc độ ghi:	10 MB/s</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '400000', '5', '1');
INSERT INTO `sanpham` VALUES ('14', 'Loa vi tính Fenda A521', '500000', '2', '2017-06-23 12:00:00', '2017-07-08 12:00:00', '4', '<UL>\n<LI>Model:	U213A</LI>\n<LI>Kích thước loa vệ tinh:	Cao 11 cm - ngang 7 cm - dày 8.3 cm</LI>\n<LI>Công suất loa vệ tinh:	3.6W</LI>\n<LI>Tổng công suất:	3.6W</LI>\n<LI>Trọng lượng:	550 g</LI>\n<LI>Cách sử dụng:	Jack cắm 3.5 mm</LI>\n<LI>Phím điều khiển:	Tăng/giảm âm lượng</LI>\n<LI>Bộ bán hàng chuẩn:	Loa</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '1', '300000', '1', '1');
INSERT INTO `sanpham` VALUES ('15', 'Ghi-ta Acoustic DVE70', '900000', '1', '2017-06-23 12:00:00', '2017-07-13 12:00:00', '5', '<UL>\n<LI>Đàn guitar Acoustic DVE70</LI>\n<LI>Mặt: gỗ Thông</LI>\n<LI>Eo + lưng: Ván ép chất lượng cao</LI>\n<LI>Cần: Thao Lao</LI>\n<LI>Mặt phím: Gõ Mật</LI>\n<LI>Ngựa: Gõ Mật</LI>\n<LI>Dây: Alice A206</LI>\n<LI>Khóa: Taiwan</LI>\nBộ sản phẩm bao gồm:	\n<LI>1 x Đàn1 x Bao vải1 x Pick1 x Bộ dây Alice A4061 x Sách hướng dẫn chơi guitar căn bản</LI>\n<LI>Kích thước sản phẩm (D x R x C cm)	107x40x11</LI>\n<LI>Trọng lượng (KG)	3</LI>\n<LI>Sản xuất tại	Việt Nam</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '2', '600000', '1', '1');
INSERT INTO `sanpham` VALUES ('16', 'Ukulele Concert', '1500000', '1', '2017-06-23 12:00:00', '2017-07-21 12:00:00', '6', '<UL>\n<LI>Ukulele Concert hay cũng được gọi Alto Ukulele có 15 đến 20 phím đàn.</LI>\n<LI>kích thước lớn hơn Soprano một chút. </LI>\n<LI>Các phím cũng xa hơn và độ căng lớn hơn. </LI>\n<LI>Nếu bạn có bàn tay lớn, thì Alto Ukulele sẽ thích hợp hơn Soprano. </LI>\n<LI>Thường được lên dây G C E A.</LI>\nBộ sản phẩm bao gồm:	\n<LI>1 x sản phẩm 1x bao 1 phiếu bảo hành</LI>\n<LI>Kích thước sản phẩm (D x R x C cm)	30 x 20 x 70</LI>\n<LI>Trọng lượng (KG)	2</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '2', '1000000', '1', '2');
INSERT INTO `sanpham` VALUES ('17', 'Ukulele Soprano', '1400000', '1', '2017-06-23 12:00:00', '2017-07-05 12:00:00', '6', '<UL>\nUkulele Soprano có kích thước nhỏ nhất, dài  21″ (khoảng 53 cm), có 12 đến 15 phím đàn. Các phím đàn Ukulele nhỏ nhắn nên phù hợp và được ưa chuộng nhiều hơn cho giới nữa cũng như các em nhỏ. \nĐàn nhỏ nhắn thích hợp khi mang theo biểu diễn văn nghệ, giải trí khi đi du lịch và dã ngoại.\nBộ sản phẩm bao gồm:	\n<LI>Đàn, bao đàn</LI>\n<LI>Kích thước sản phẩm (D x R x C cm)	55x25x10</LI>\n<LI>Trọng lượng (KG)	1</LI>\n<LI>Sản xuất tại	Việt Nam</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '2', '1000000', '2', '3');
INSERT INTO `sanpham` VALUES ('18', 'Ghi-ta Classic Yamaha CG122MS', '5900000', '1', '2017-06-23 12:00:00', '2017-07-08 12:00:00', '5', '<UL>\nDòng model có mặt đàn bằng gỗ nguyên tấm này có mức giá phải chăng. Đây là model Mat Finish duy nhất trong dòng sản phẩm này và chất lượng âm thanh của nó chắc chắn là nổi bật ở mức giá này.\nBộ sản phẩm bao gồm:	\n<LI>1 x Sản phẩm</LI>\n<LI>Màu	Gỗ</LI>\n<LI>Kích thước sản phẩm (D x R x C cm)	110 x 40 x 15</LI>\n<LI>Trọng lượng (KG)	3</LI>\n<LI>Sản xuất tại	Indonesia</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '2', '3500000', '2', '5');
INSERT INTO `sanpham` VALUES ('19', 'Ghi-ta Classic RG3920 ', '1000000', '1', '2017-06-23 12:00:00', '2017-07-06 12:00:00', '5', '<UL>\n<LI>Xuất Xứ: Trung Quốc</LI>\n<LI>Màu Sắc: Vàng</LI>\n<LI>Mặt Đàn:  Spruce</LI>\n<LI>Lưng & Hông: Sapelle</LI>\n<LI>Đầu Đàn & Cần: Nato</LI>\n<LI>Ngựa Đàn: Nhựa cao cấp </LI>\n<LI>Dây Đàn: Alice A406</LI>\nBộ sản phẩm bao gồm:	\n<LI>1 x Sản phẩm</LI>\n<LI>Mẫu mã	RG3920</LI>\n<LI>Kích thước sản phẩm (D x R x C cm)	105 x 10 x 40</LI>\n<LI>Trọng lượng (KG)	2.9</LI>\n<LI>Sản xuất tại	Trung Quốc</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '2', '600000', '3', '2');
INSERT INTO `sanpham` VALUES ('20', 'Ghi-ta Điện Yamaha Pacifica 112V', '8500000', '1', '2017-06-23 12:00:00', '2017-07-05 12:00:00', '5', '<UL>\nMột trong những guitar electric (guitar điện)giá trị tốt nhất trong nhiều thập niên qua, Đàn Yamaha Pacifica cũng nổi tiếng về âm tốt và tính dễ chơi nổi bật. Pacifica Series có đặc điểm là phần thân đàn viền đẹp, thiết kế cần đàn thường, bộ rung kiểu cổ điển, và các chuyển đổi cấu hình pickup H-S-S 5 chiều.\nBộ sản phẩm bao gồm:	\n<LI>1 x sản phẩm</LI>\n<LI>Kích thước sản phẩm (D x R x C cm)	100 x 40 x 5</LI>\n<LI>Trọng lượng (KG)	3</LI>\n<LI>Sản xuất tại	Indonesia</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '2', '6500000', '4', '1');
INSERT INTO `sanpham` VALUES ('21', 'Trống lắc tay Tambourine Yamaha', '450000', '1', '2017-06-23 12:00:00', '2017-07-03 12:00:00', '7', '<UL>\n<LI>Số lượng chuông kép: 14</LI>\n<LI>Đường kính khung: 26cm</LI>\n<LI>Khung nhựa mạ bạc với các chốt căng mặt trống bằng Inox giúp điều chỉnh độ căng mặt trống</LI>\n<LI>Đây là loại Tambourine chất lượng với giá thành phải chăng </LI>\n<LI>Độ bền cao</LI>\n<LI>Hình thức đẹp mắt</LI>\nBộ sản phẩm bao gồm:	\n<LI>1 x sản phẩm</LI>\n<LI>Kích thước sản phẩm (D x R x C cm)	26 x 26 x 5</LI>\n<LI>Trọng lượng (KG)	0.3</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '2', '280000', '1', '3');
INSERT INTO `sanpham` VALUES ('22', 'Trống điện từ M1008', '899000', '1', '2017-06-23 12:00:00', '2017-07-11 12:00:00', '7', '<UL>\nSản phẩm trống điện tử Drum Kit được phân phối độc quyền bởi thương hiệu StarMart.\nVới bộ trống chất liệu silicon thân thiện môi trường, bé nhà bạn có thể dễ dàng mang theo đến bất kì đâu, thỏa thích cùng bạn bè vui chơi các bữa tiệc âm nhạc đầy âm thanh và sắc màu. Bộ có 9 loại trống với âm thanh chân thật không thua gì trống điện tử, sẽ là món quà tuyệt vời cho bé nhà bạn phát triển tài năng âm nhạc.\nBộ sản phẩm bao gồm:	\n1 trống điện tử 2 dùi trống 1 USB cáp 2 bàn đạp chân 1 hướng dẫn sử dụng\n<LI>Mẫu mã	TRỐNG ĐIỆN TỬ MODEL M1008</LI>\n<LI>Kích thước sản phẩm (D x R x C cm)	57*31*2.5</LI>\n<LI>Trọng lượng (KG)	0.96</LI>\n<LI>Sản xuất tại	Trung Quốc, Việt Nam</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.', '2', '600000', '2', '3');
INSERT INTO `sanpham` VALUES ('23', 'Dùi chổi đánh trống Cajon', '150000', '2', '2017-06-23 12:00:00', '2017-07-13 12:00:00', '7', '<UL>\n<LI>Dùi chổi chuyên đánh trống Cajon.</LI>\n<LI>Có 2 màu: màu xanh và màu trắng.</LI>\n<LI>Chất liệu: nilong cao cấp.</LI>\n<LI>Dễ sử dụng, gọn nhẹ, có thể kéo ra kéo vào tùy thích.</LI>\n<LI>Tạo ra âm thanh hay bảo đảm bạn sẽ thích.</LI>\n<LI>Bộ sản phẩm bao gồm:	</LI>\n<LI>1x 1 cặp dùi chổi</LI>\n<LI>Mẫu mã	Dùi chổi trống Cajon ( Màu xanh và màu trắng)</LI>\n<LI>Kích thước sản phẩm (D x R x C cm)	4x2x23</LI>\n<LI>Trọng lượng (KG)	0.1</LI>\n<LI>Sản xuất tại	Đài Loan, Trung Quốc</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '2', '80000', '3', '3');
INSERT INTO `sanpham` VALUES ('24', 'Trống Cajon', '1600000', '1', '2017-06-23 12:00:00', '2017-07-08 12:00:00', '7', '<UL>\n<LI>Bộ sản phẩm bao gồm:</LI>	\n<LI>1 x sản phẩm</LI>\n<LI>Mẫu mã	Sol.G (Hà Nội)-SGT05</LI>\n<LI>Kích thước sản phẩm (D x R x C cm)	30 x 30 x 42</LI>\n<LI>Trọng lượng (KG)	3</LI>\n<LI>Sản xuất tại	Việt Nam</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không còn bảo hành.\nTest tại chỗ.', '2', '800000', '4', '4');
INSERT INTO `sanpham` VALUES ('25', 'Áo khoác Kaki bomber', '170000', '2', '2017-06-23 12:00:00', '2017-07-08 12:00:00', '8', 'Được thiết kế với kiểu dáng thời trang đẹp mắt, phù hợp với mọi dáng người, dễ phối đồ, dễ đẹp.', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '100000', '1', '5');
INSERT INTO `sanpham` VALUES ('26', 'Áo Khoác Hoodie', '220000', '2', '2017-06-23 12:00:00', '2017-07-06 12:00:00', '8', '<UL>\n<LI>Trẻ trung, năng động, nam tính và sành điệu cùng Giày Sneakernam dáng xỏ của nhà phân phối Hanama.</LI> \n<LI>Tông đen sang trọng, đẳng cấp cùng chất liệu da lộn cao cấp, mềm mại, không mang đến cảm giác đau chân khi phải di chuyển nhiều, sản phẩm tạo nên sự tin tưởng tuyệt đối trong sự lựa chọn của giới trẻ thời nay.</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '160000', '2', '2');
INSERT INTO `sanpham` VALUES ('27', 'Biti\'s Hunter', '650000', '1', '2017-06-23 12:00:00', '2017-07-05 12:00:00', '10', '<UL>\n<LI>Sở hữu nhiều cải tiến về công nghệ lẫn thiết kế, hứa hẹn sẽ gặt hái nhiều thành công.</LI> \n<LI>So với sản phẩm trước, phiên bản mới có sự thay đổi ở chất liệu quai dệt.</LI>\n<LI>Áp dụng công nghệ quai dệt mới có tên gọi Liteknit, có lỗ thoát khí giúp tạo sự thông thoáng. </LI>\n<LI>Bên cạnh đó, giày có kiểu dáng và cấu trúc mới đáp ứng các yêu cầu năng động giới trẻ, phù hợp với các hoạt động thể thao và sử dụng thường nhật. </LI>\n<LI>Phần đế từ chất liệu phylon siêu nhẹ, kết hợp đế tiếp đất cao su tạo sự ma sát tốt, chống trơn trượt, giúp người mang di chuyển thoải mái và dễ dàng.</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '400000', '1', '4');
INSERT INTO `sanpham` VALUES ('28', 'Chân váy xòe dài vintage', '79000', '1', '2017-06-23 12:00:00', '2017-06-29 12:00:00', '9', '<UL>\n <LI>Chân váy xòe dài vintage 2 túi với form dáng rũ lý tưởng cùng màu xanh navy nổi bật, giúp bạn gái dễ diện cùng nhiều kiểu áo và phụ kiện khác nhau.</LI>\n <LI>Chân váy có độ dài qua gối, được may một màu trơn đơn giản nhưng tinh tế, ấn tượng với túi mổ hai bên lạ mắt, độc đáo.</LI>\n <LI>Thích hợp diện khi đến nơi công sở, dạo chơi cùng bạn bè, đi biển,...</LI>\n<LI> Được may từ chất vải tuyết mưa mềm mịn, thoáng mát, tạo cảm giác thoải mái khi mặc.</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '50000', '1', '4');
INSERT INTO `sanpham` VALUES ('29', 'Đầm xòe phối lưới viền đen', '190000', '2', '2017-06-23 12:00:00', '2017-06-27 12:00:00', '9', '<UL>\n<LI>Lột mặt trái sản phẩm khi giặt, phơi</LI>\n<LI>Không sử dụng thuốc tẩy.</LI>\n<LI>Là ở nhiệt độ thấp.Vai x Vòng Ngực x Vòng Eo x Dài:</LI>\n<LI>Size S: 36 x 84 x 68 x 56</LI>\n<LI>Size M: 37 x 88 x 72 x 58</LI>\n<LI>Size L: 38 x 92 x 76 x 60</LI>\n<LI>Vòng bụng x mông x dài:</LI>\n<LI>Size S: 70 x 86 x 30</LI>\n<LI>Size M: 72 x 90 x 32</LI>\n<LI>Size L: 74 x 94 x 34</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '120000', '2', '2');
INSERT INTO `sanpham` VALUES ('30', 'Đầm trễ vai sọc ngang', '63000', '1', '2017-06-23 12:00:00', '2017-07-10 12:00:00', '9', '<UL>\n<LI>Đầm trễ vai sọc ngang cho nữ</LI>\n<LI>Chất liệu : Thun cát</LI>\n<LI>Size: freesize (45 54 kg)</LI>\n<LI>Chất vải mềm; thoáng mát; thấm hút mồ hôi tốt; bền màu</LI>\n<LI>Phong cách đơn giản; dễ phối đồ</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '40000', '3', '6');
INSERT INTO `sanpham` VALUES ('31', 'Áo khoác nữ kaki phối nón', '139000', '1', '2017-06-23 12:00:00', '2017-07-08 12:00:00', '9', '<UL>\n<LI>Chất liệu cao cấp, thoáng mát, họa tiết tinh xảo đem đến cho bạn gái vẻ ngoài duyên dáng, cuốn hút</LI>\n<LI>Bảo vệ bạn gái khỏi cái nắng gay gắt, hay những lúc trời trở lạnh, mang đến cho các bạn vẻ năng động</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '90000', '4', '2');
INSERT INTO `sanpham` VALUES ('32', 'Đầm sọc caro', '120000', '1', '2017-06-23 12:00:00', '2017-07-09 12:00:00', '9', '<UL>\n<LI>Đầm sọc caro kute đang là mẫu đầm được các bạn trẻ rất ưa chuông hiện nay.</LI>\n<LI>Mẫu đầm giúp các bạn có thể khoe những vòng eo đẹp, một mặt khác chiếc đầm caro rất dễ phối đồ với những chếc bóp cầm tay hay chéo lưng, những mẫu đồng hồ xten, hay những đôi giày dễ thương. </LI>\n<LI>Chất vải thun cát cao cấp tạo cảm giác thoải mái khi mặc, kiểu dáng free size phù hợp cho những bạn có cân nặng từ 45-55kg tùy chiều cao</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '70000', '5', '1');
INSERT INTO `sanpham` VALUES ('33', 'Đầm ren chữ A', '169000', '2', '2017-06-23 12:00:00', '2017-07-06 12:00:00', '9', 'Dễ thương, kiểu dáng thời trang. Dễ dàng phối trang phục.', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '100000', '6', '2');
INSERT INTO `sanpham` VALUES ('34', 'Nón Alan Walker', '150000', '2', '2017-06-23 12:00:00', '2017-07-01 12:00:00', '10', 'Nón chất liệu Kaki  Mẫu nón này được giới trẻ yêu thích  kiểu dáng năng động\n', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '80000', '2', '3');
INSERT INTO `sanpham` VALUES ('35', 'Mũ cói vành nhỏ đính nơ', '120000', '1', '2017-06-23 12:00:00', '2017-07-08 12:00:00', '10', '<UL>\n<LI>Mũ cói vành nhỏ thắt nơ xinh xắn nổi bật, dễ thương giúp bạn che nắng, bảo vệ da, thể hiện phong cách, cá tính</LI>\n<LI>Thích hợp để dạo phố, đi biển, dã ngoại cùng bạn bè</LI>\n<LI>Được làm từ chất liệu cói, tạo cảm giác thông thoáng khi đội, độ bền cao</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '60000', '3', '4');
INSERT INTO `sanpham` VALUES ('36', 'Găng tay chống nắng', '60000', '1', '2017-06-23 12:00:00', '2017-07-06 12:00:00', '10', '<UL>\n<LI>Ống tay chống nắng loại tốt korea</LI>\n<LI>Chất liệu bền co giãn mịn</LI>\n<LI>Chống nắng </LI>\n<LI>Thiết kế đẹp</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '20000', '4', '1');
INSERT INTO `sanpham` VALUES ('37', 'Thắt lưng nam da', '220000', '1', '2017-06-23 12:00:00', '2017-07-08 12:00:00', '10', '<UL>\n<LI>Chất liệu da cao cấp</LI>\n<LI>Kiểu dáng nam tính; thanh lịch</LI>\n<LI>Phù hợp nhiều trang phục</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '150000', '5', '2');
INSERT INTO `sanpham` VALUES ('38', 'Cà vạt nam nhỏ', '100000', '2', '2017-06-23 12:00:00', '2017-07-08 12:00:00', '10', '<UL>\n<LI>Chất liệu:polyester</LI>\n<LI>Thiết kế thanh lịch</LI>\n<LI>Màu sắc nhã nhặn</LI>\n<LI>Phù hợp với nhiều trang phục</LI>\n<LI>Kích thước:145x5x3 cm</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '3', '50000', '6', '2');
INSERT INTO `sanpham` VALUES ('39', 'Kem nền dạng gel', '146000', '1', '2017-06-23 12:00:00', '2017-07-07 12:00:00', '11', '<UL>\n<LI>Giữ ẩm gấp 2 lần</LI>\n<LI>Bền màu suốt 12h</LI>\n<LI>Cho lớp nền mịn mượt như nhung</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '4', '100000', '1', '3');
INSERT INTO `sanpham` VALUES ('40', 'Son môi NYX', '168000', '2', '2017-06-23 12:00:00', '2017-07-04 12:00:00', '11', '<UL>\n<LI>Thành phần an toàn</LI>\n<LI>Lên màu đẹp</LI>\n<LI>Lâu trôi</LI>\n<LI>Dành cho bạn gái</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '4', '80000', '2', '4');
INSERT INTO `sanpham` VALUES ('41', 'Mascara', '99000', '1', '2017-06-23 12:00:00', '2017-07-03 12:00:00', '11', '<UL>\n<LI>Mascara làm dày và cong mi</LI>\n<LI>9.2ml</LI>\n<LI>Thương hiệu Mỹ</LI>\n<LI>Chi tiết bao bì có thể thay đổi theo từng đợt nhập hàng</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '4', '60000', '3', '3');
INSERT INTO `sanpham` VALUES ('42', 'Bộ dụng cụ làm móng', '45400', '1', '2017-06-23 12:00:00', '2017-06-25 12:00:00', '11', '<UL>\n<LI>Chất liệu hợp kim không gỉ</LI>\n<LI>Thiết kế chắc chắn bền bỉ</LI>\n<LI>Hộp đựng sang trọng tiện ích</LI>\n<LI>Bộ sản phẩm gồm 12 món đa dạng</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '4', '25000', '4', '3');
INSERT INTO `sanpham` VALUES ('43', 'Viên uống tăng cường sinh lực Alltimes Care', '800000', '2', '2017-06-23 12:00:00', '2017-07-09 12:00:00', '12', '<UL>\n<LI>Sản phẩm không phải là thuốc và không có tác dụng thay thế thuốc chữa bệnh</LI>\n<LI>Chứa Maca 500mg</LI>\n<LI>Tăng cường chức năng sinh lý; giảm nguy cơ ung thư tiền liệt tuyến; hỗ trợ xương khớp ở nam giới</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '4', '600000', '1', '7');
INSERT INTO `sanpham` VALUES ('44', 'Viên uống kích thích ăn ngon Puritan\'s Pride', '124000', '1', '2017-06-23 12:00:00', '2017-07-04 12:00:00', '12', '<UL>\n<LI>Sản phẩm không phải là thuốc; không có tác dụng thay thế thuốc chữa bệnh\n<LI>Kích thích ăn ngon miệng; tăng cường cơ bắp chắc khỏe\n<LI>100 viên\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '4', '90000', '2', '7');
INSERT INTO `sanpham` VALUES ('45', 'Viên uống bổ mắt WIT', '290000', '1', '2017-06-23 12:00:00', '2017-07-06 12:00:00', '12', '<UL>\n<LI>Tăng cường thị lực, bảo vệ mắt, sáng mắt</LI>\n<LI>Chống thái hóa điểm vàng</LI>\n<LI>Tăng cường não bộ</LI>\n<LI>Đẹp da</LI>\n<LI>Mỗi ngày một viên cho đôi mắt sáng khỏe</LI>\n<LI>Chiết xuất từ vi tảo lục Haematoccccus pluvialis Nhật Bản</LI>\n<LI>Hợp chất tự nhiên Lutein, Vitamin A, Vitamin B2, Billberry Extract chống oxy hóa mạnh</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '4', '200000', '3', '7');
INSERT INTO `sanpham` VALUES ('46', 'Xe lưu động dã ngoại LEGO', '399000', '1', '2017-06-23 12:00:00', '2017-07-06 12:00:00', '13', '<UL>\n<LI>Gồm 250 chi tiết</LI>\n<LI>Chất liệu cao cấp & an toàn</LI>\n<LI>Kích thích tư duy; sáng tạo</LI>\n<LI> Trẻ Trên 3 tuổi</LI>\n<LI>Rèn luyện sự khéo léo</LI>\n<LI>Chi tiết bao bì có thể thay đổi theo đợt nhập hàng</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '5', '250000', '1', '7');
INSERT INTO `sanpham` VALUES ('47', 'Bộ ráp LEGO Classic 10709', '80000', '1', '2017-06-23 12:00:00', '2017-07-08 12:00:00', '13', '<UL>\n<LI>Chất liệu nhựa nguyên sinh ABS cao cấp</LI>\n<LI>Gồm 66 mảnh ghép</LI>\n<LI>Màu sắc nổi bật; tươi sáng</LI>\n<LI>Kích thích thị giác và khả năng sáng tạo của bé</LI>\n<LI>Độ tuổi: 4 tuổi trở lên</LI>\n<LI>Giúp sự phối hợp giữa tay & mắt trở nên linh hoạt</LI>\n<LI>Chi tiết bao bì có thể thay đổi theo từng đợt nhập hàng</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '5', '50000', '2', '7');
INSERT INTO `sanpham` VALUES ('48', 'Đồ chơi gỗ ngựa bập bênh', '558000', '1', '2017-06-23 12:00:00', '2017-07-08 12:00:00', '14', '<UL>\n<LI>Đồ Chơi Gỗ Ngựa Bập Bênh Vietoys VTU3-0019 được thiết kế đẹp mắt, sinh động sẽ mang đến cho bé những giờ phút vui chơi thoải mái, hấp dẫn. </LI>\n<LI>Được làm từ chất liệu gỗ cao cấp, bền không chứa chất độc hại, sản phẩm đảm bảo an toàn tối ưu cho bé khi sử dụng.</LI>\n<LI>Các chi tiết của ngựa gỗ được bo tròn và tính toán theo tỉ lệ hợp lý cho bé ngồi chơi thoải mái mà không lo té ngã.</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '5', '300000', '1', '3');
INSERT INTO `sanpham` VALUES ('49', 'Đồ chơi gỗ mô hình bàn ghế', '240000', '1', '2017-06-23 12:00:00', '2017-07-12 12:00:00', '14', '<UL>\n<LI>Đồ chơi Gỗ Mô Hình EDUGAMES Bộ Bàn Ghế, Giường Tủ - GA514 giúp bé có thể nhận biết được các đồ vật hàng ngày trong gia đình. Ngoài ra, mẹ có thể cùng bé cùng chơi với bộ bàn ghế, giường tủ Edugames, mô phỏng không khí gia đình cho bạn búp bê xinh xắn.</LI>\n<LI>Gồm giường, tủ, bàn tiếp khách, ghế dài, hai ghế đơn làm bằng ván ép MDF.</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '5', '160000', '2', '2');
INSERT INTO `sanpham` VALUES ('50', 'Nendoroid Rem', '330000', '1', '2017-06-23 12:00:00', '2017-07-20 12:00:00', '15', 'Búp bê của Nhật.', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '5', '220000', '1', '1');
INSERT INTO `sanpham` VALUES ('51', 'Bếp búp bê loại lớn', '380000', '1', '2017-06-23 12:00:00', '2017-07-15 12:00:00', '15', '<UL>\n<LI>Bếp búp bê loại lớn với kích thước dài 50cmx40cmx15cm với những phụ kiện nhà bếp đi kèm</LI>\n<LI>Xài bằng pin, sáng đèn bếp</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '5', '250000', '2', '2');
INSERT INTO `sanpham` VALUES ('52', 'Gấu Teddy Socola 1m4', '480000', '2', '2017-06-23 12:00:00', '2017-07-22 12:00:00', '15', 'Thú nhồi bông lớn với nhiều kích thước', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '5', '300000', '3', '3');
INSERT INTO `sanpham` VALUES ('53', 'Phong Thần Reload - Tập 1', '35000', '2', '2017-06-23 12:00:00', '2017-08-05 12:00:00', '0', 'Tiên nhân trẻ tuổi đột nhiên xuất hiện trước mặt thiếu nữ, nhờ cô giúp đỡ tiêu diệt hồ yêu, đặt ra một cái bẫy cực kỳ nguy hiểm. Hồ yêu đẹp trai với vẻ ngoài hiền lành vô hại, tự nhận có mối ân oán bất minh với vị tiên trẻ, có thật sự là yêu quái tà ác? Bảo vệ và phản bội, khát vọng làm tiên và tình yêu thuần khiết, chính tà giao tranh... cuối cùng sẽ kết thúc như thế nào?', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '6', '20000', '1', '2');
INSERT INTO `sanpham` VALUES ('54', 'Tuổi Trẻ Đáng Giá Bao Nhiêu', '50000', '2', '2017-06-23 12:00:00', '2017-07-28 12:00:00', '0', 'Grace và Connor phát hiện ra bí mật lớn nhất đời mình: chúng là Ma cà rồng lai người. cuộc phiêu lưu lại tiếp tục nhưng chúng không tiếp tục cùng nhau mà chia thành hai con đường song song, hai lý tưởng, hai cuộc đời từ đây sẽ có nhiều thay đổi.', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '6', '30000', '2', '2');
INSERT INTO `sanpham` VALUES ('55', 'Isaac - Những Ngày Nắng Đẹp', '120000', '1', '2017-06-23 12:00:00', '2017-07-06 12:00:00', '0', '12 truyện ngắn trong 12 cách yêu là những cốt truyện độc đáo, ly kỳ được nhào nặn tài tình bởi cây bút \"vạn vạn bản\" Hamlet Trương chắc chắn sẽ khiến bạn không dứt mắt ra khỏi tác phẩm này. \"12 cách yêu\" được sáng tác trong ròng rã 3 năm và cũng là cuốn truyện ngắn đầu tay của Hamlet Trương sau những thành công ở thể lọai Tản văn.', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '6', '80000', '3', '2');
INSERT INTO `sanpham` VALUES ('56', 'Đĩa cài Win 7', '75000', '1', '2017-06-23 12:00:00', '2017-07-07 12:00:00', '0', '<UL>\n<LI>Tiện lợi</LI>\n<LI>Có thể dễ dàng thao tác</LI>\n<LI>Cài lại win cho máy tính hoặc laptop</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '8', '50000', '1', '2');
INSERT INTO `sanpham` VALUES ('57', 'Phần mềm diệt Virus BKAV Pro', '151600', '1', '2017-06-23 12:00:00', '2017-07-28 12:00:00', '0', '<UL>\n<LI>Tiên phong sử dụng công nghệ điện toán đám mây</LI>\n<LI>Sản phẩm cấp quốc tế</LI>\n<LI>Phần mềm tốt nhất do Hiệp hội An toàn thông tin Việt Nam bình chọn</LI>\n<LI>Có nhiều tính năng ưu việt</LI>\n<LI>Được trang bị công nghệ đột phá Safe Run- thực thi an toàn</LI>\n<LI>Có tính năng tự cập nhật hoàn toàn tự động</LI>\n<LI>Có bản quyền</LI>\n<LI>Được hỗ trợ trực tiếp từ các chuyên gia hàng đầu</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '8', '100000', '2', '3');
INSERT INTO `sanpham` VALUES ('58', 'Bình đun siêu tốc Comet', '99000', '1', '2017-06-23 12:00:00', '2017-07-19 12:00:00', '0', '<UL>\n<LI>Dung tích bình lớn 1.5 Lít</LI>\n<LI>Công suất nấu 1500W đun sôi từ 4 - 6 phút</LI>\n<LI>Tự ngắt khi nước sôi và khi cạn nước</LI>\n<LI>Ruột bình và mâm nhiệt bằng inox</LI>\n<LI>Nắp bình được thiết kế dễ dàng đóng mở</LI>\n<LI>Đèn báo hoạt động tiện dụng</LI>\n<LI>Đế tiếp điện xoay 360 độ</LI>\n<LI>Tay cầm bằng nhựa cách nhiệt, chống bỏng</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '7', '60000', '1', '4');
INSERT INTO `sanpham` VALUES ('59', 'Tủ lạnh Aqua AQR', '2399000', '1', '2017-06-23 12:00:00', '2017-07-12 12:00:00', '0', '<UL>\n<LI>Công suất 64W</LI>\n<LI>Dung tích 90L</LI>\n<LI>Công nghệ làm lạnh trực tiếp</LI>\n<LI>Tiết kiệm điện năng; An toàn cho sức khỏe</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '7', '1600000', '2', '5');
INSERT INTO `sanpham` VALUES ('60', 'Bàn ủi hơi nước Electrolux', '289000', '1', '2017-06-23 12:00:00', '2017-07-05 12:00:00', '0', '<UL>\n<LI>Công suất 1500W</LI>\n<LI>Mắt đề bằng hợp kim nhôm cao cấp</LI>\n<LI>Hai chế độ phun thông minh</LI>\n<LI>Hoạt động bền bỉ an toàn</LI>\n</UL>', '<UL>\n<LI> Add: 114 Đường Số 2 Cư Xá Đô Thành P.4 Q.3</LI>\n<LI>  ĐT : 0969 11 13 15</LI>\n<LI> Vũ Ngọc Quang</LI>\n<LI> TK Vietcombank : 0181000518524</LI>\n<LI> Phí ship hàng: thỏa thuận</LI>\n<LI> Ship COD sẽ = phí ship + thêm 10k</LI>\n</UL>\nBạn nào thắng không nhận hàng vì lý do ABC gì đó vui lòng hoàn lại phí 20k để mình đấu giá lại.Thanks\nLưu ý : Thời gian nhận hàng 48h. Quá thời gian mà không nhận hàng sẽ bị ban nick', 'Không có chế độ bảo hành.', '7', '160000', '3', '6');

-- ----------------------------
-- Table structure for users
-- ----------------------------
DROP TABLE IF EXISTS `users`;
CREATE TABLE `users` (
  `userID` int(11) NOT NULL AUTO_INCREMENT,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `name` varchar(255) NOT NULL,
  `adress` varchar(255) DEFAULT NULL,
  `DOB` date DEFAULT NULL,
  `permission` int(11) DEFAULT NULL,
  `timestamp` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`userID`,`email`),
  KEY `userID` (`userID`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;

-- ----------------------------
-- Records of users
-- ----------------------------
INSERT INTO `users` VALUES ('1', 'hieuphan@gmail.com', '246c9becbd776764242cc2f649dc2a00', 'Phan Trung Hieu', '60 Hoang Hoa Tham', '2017-06-01', '1', '2017-06-14 23:57:48');
INSERT INTO `users` VALUES ('2', 'pthieu@gmail.com', '246c9becbd776764242cc2f649dc2a00', 'Natsudragoneel', '61 Hoang Hoa Tham', '2017-06-22', '0', '2017-06-06 23:57:48');
INSERT INTO `users` VALUES ('4', 'pthieu@gmail.com', '246c9becbd776764242cc2f649dc2a00', 'phan trung hieu', '60 Hoàng Hoa Thám', '2017-06-01', '0', '2017-06-28 23:57:48');
INSERT INTO `users` VALUES ('6', 'hieu222222222@gmail.com', '246c9becbd776764242cc2f649dc2a00', 'tai gam hieu', '60 Hoàng Hoa Thám', '1995-01-01', '1', '2017-06-28 23:57:48');
INSERT INTO `users` VALUES ('7', 'eminem@gmail.com', '246c9becbd776764242cc2f649dc2a00', 'hieuphan', '60 Hoàng Hoa Thám', '1994-01-18', '0', '2017-06-28 23:57:48');
INSERT INTO `users` VALUES ('8', 'red@gmail.com', '246c9becbd776764242cc2f649dc2a00', 'Red', '60 Hoàng Hoa Thám', '1998-06-30', '0', '2017-06-30 09:16:57');
INSERT INTO `users` VALUES ('9', 'yellow@gmail.com', '246c9becbd776764242cc2f649dc2a00', 'Yellow', '60 Hoàng Hoa Thám', '2017-06-13', '0', '2017-06-30 09:19:22');
INSERT INTO `users` VALUES ('10', 'blue@gmail.com', '246c9becbd776764242cc2f649dc2a00', 'Blue', '60 Hoàng Hoa Thám', '2017-06-05', '0', '2017-06-30 09:19:53');
INSERT INTO `users` VALUES ('11', 'white@gmail.com', '246c9becbd776764242cc2f649dc2a00', 'White', '60 Hoàng Hoa Thám', '2017-05-31', '0', '2017-06-30 09:20:11');
INSERT INTO `users` VALUES ('12', 'black@gmail.com', '246c9becbd776764242cc2f649dc2a00', 'Black', '60 Hoàng Hoa Thám', '2017-06-05', null, '2017-06-30 09:20:30');

-- ----------------------------
-- Table structure for yeuthich
-- ----------------------------
DROP TABLE IF EXISTS `yeuthich`;
CREATE TABLE `yeuthich` (
  `userID` int(11) NOT NULL,
  `idsanpham` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of yeuthich
-- ----------------------------
INSERT INTO `yeuthich` VALUES ('7', '1');
INSERT INTO `yeuthich` VALUES ('7', '2');
INSERT INTO `yeuthich` VALUES ('1', '1');
