var mustache = require('mustache'),
    q = require('q'),
    db = require('../fn/db');
exports.loadTop5Price = function() {
    var sql = 'SELECT * FROM (SELECT bid.idsanpham, bid.giaMua, nguoimua.namenguoimua FROM (SELECT idsanpham, MAX(gia) AS giaMua FROM bid_log GROUP BY idsanpham) AS bid, bid_log, nguoimua WHERE bid.giaMua = bid_log.gia AND bid.idsanpham = bid_log.idsanpham AND bid_log.nguoimua = nguoimua.idnguoimua) AS giaMax JOIN sanpham ON giaMax.idsanpham = sanpham.idsanpham WHERE NOW() < timeend ORDER BY giaMua DESC LIMIT 0,5';
    return db.load(sql);
}
exports.loadTop5Bid = function() {
    var sql = 'SELECT * FROM (SELECT bid.idsanpham, bid.giaMua, nguoimua.namenguoimua FROM (SELECT idsanpham, MAX(gia) AS giaMua FROM bid_log GROUP BY idsanpham) AS bid, bid_log, nguoimua WHERE bid.giaMua = bid_log.gia AND bid.idsanpham = bid_log.idsanpham AND bid_log.nguoimua = nguoimua.idnguoimua) AS giaMax JOIN sanpham ON giaMax.idsanpham = sanpham.idsanpham WHERE NOW() < timeend ORDER BY Bid DESC LIMIT 0,5';
    return db.load(sql);
}
exports.loadTop5Date = function() {
    var sql = 'SELECT * FROM (SELECT * FROM (SELECT idsanpham AS idsp, DATEDIFF(timeend, CURDATE()) AS datediff FROM sanpham) AS date, sanpham WHERE sanpham.idsanpham = date.idsp) AS topdate JOIN (SELECT bid.idsanpham, bid.giaMua, nguoimua.namenguoimua FROM (SELECT idsanpham, MAX(gia) AS giaMua FROM bid_log GROUP BY idsanpham) AS bid, bid_log, nguoimua WHERE bid.giaMua = bid_log.gia AND bid.idsanpham = bid_log.idsanpham AND bid_log.nguoimua = nguoimua.idnguoimua) AS giaMax ON topdate.idsanpham = giaMax.idsanpham WHERE NOW() < timeend ORDER BY datediff LIMIT 0,5';
    return db.load(sql);
}
