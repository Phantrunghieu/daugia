var mustache = require('mustache'),
    Q=require('q'),
    db = require('../fn/db');
exports.loadPageByName = function(name, limit, offset) {

    var deferred = Q.defer();

    var promises = [];

    var view = {
        Name: name,
        limit: limit,
        offset: offset
    };
    var sqlCount = mustache.render('select count(*) as total FROM sanpham LEFT JOIN (SELECT nguoimua.namenguoimua,sanpham.idsanpham FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.`name` LIKE "%{{Name}}%") AS B ON sanpham.idsanpham=B.idsanpham where sanpham.`name` LIKE "%{{Name}}%"', view);
    promises.push(db.load(sqlCount));
    var sql1 = mustache.render('select * from (select * from sanpham LEFT JOIN (SELECT bid.gia,nguoimua.namenguoimua,sanpham.idsanpham AS id FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.`name` LIKE "%{{Name}}%") AS B ON sanpham.idsanpham=B.id where sanpham.`name` LIKE "%{{Name}}%") AS T, (SELECT idsanpham AS idsp, DATEDIFF(timeend, CURDATE()) AS datediff FROM sanpham) AS H WHERE T.idsanpham = H.idsp ORDER BY(datediff)  limit {{limit}} offset {{offset}}', view);
    promises.push(db.load(sql1));
    var sql2 = mustache.render('select * from sanpham LEFT JOIN (SELECT bid.gia,nguoimua.namenguoimua,sanpham.idsanpham AS id FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.`name` LIKE "%{{Name}}%") AS B ON sanpham.idsanpham=B.id where sanpham.`name` LIKE "%{{Name}}%" ORDER BY(gia) limit {{limit}} offset {{offset}}', view);
    promises.push(db.load(sql2));
    var sql3 = mustache.render('select * from sanpham LEFT JOIN (SELECT bid.gia,nguoimua.namenguoimua,sanpham.idsanpham AS id FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.`name` LIKE "%{{Name}}%") AS B ON sanpham.idsanpham=B.id where sanpham.`name` LIKE "%{{Name}}%" ORDER BY(gia) DESC limit {{limit}} offset {{offset}}', view);
    promises.push(db.load(sql3));
    Q.all(promises).spread(function(totalRow, cRows, dRows, pRows) {
        var data = {
            total: totalRow[0].total,
            list1: cRows,
            list2: dRows,
            list3: pRows
        }
        deferred.resolve(data);
    });

    return deferred.promise;
}
exports.loadPageByCatID = function(id1,id2, limit, offset) {

    var deferred = Q.defer();

    var promises = [];

    var view = {
        Id1: id1,//danhmuccon
        Id2: id2,//danhmuc
        limit: limit,
        offset: offset
    };
    
    if(id1 === 0)
    {
        var sqlCount = mustache.render('select count(*) as total FROM sanpham LEFT JOIN (SELECT nguoimua.namenguoimua,sanpham.idsanpham FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.danhmuc={{Id2}}) AS B ON sanpham.idsanpham=B.idsanpham WHERE sanpham.danhmuc={{Id2}}', view);
        promises.push(db.load(sqlCount));
        var sql1 = mustache.render('SELECT * FROM (SELECT * FROM sanpham LEFT JOIN (SELECT bid.gia,nguoimua.namenguoimua,sanpham.idsanpham as id FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}}) AS B ON sanpham.idsanpham=B.id WHERE sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}}) AS T, (SELECT idsanpham AS idsp, DATEDIFF(timeend, CURDATE()) AS datediff FROM sanpham) AS H WHERE T.idsanpham = H.idsp ORDER BY(datediff) limit {{limit}} offset {{offset}}', view);
        promises.push(db.load(sql1));
        var sql2 = mustache.render('SELECT * FROM sanpham LEFT JOIN (SELECT bid.gia,nguoimua.namenguoimua,sanpham.idsanpham as id FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}}) AS B ON sanpham.idsanpham=B.id WHERE sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}} ORDER BY(gia) limit {{limit}} offset {{offset}}', view);
        promises.push(db.load(sql2));
        var sql3 = mustache.render('SELECT * FROM sanpham LEFT JOIN (SELECT bid.gia,nguoimua.namenguoimua,sanpham.idsanpham as id FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}}) AS B ON sanpham.idsanpham=B.id WHERE sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}} ORDER BY(gia) DESC limit {{limit}} offset {{offset}}', view);
    }
    else
    {   
        var sqlCount = mustache.render('select count(*) as total FROM sanpham LEFT JOIN (SELECT nguoimua.namenguoimua,sanpham.idsanpham FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}}) AS B ON sanpham.idsanpham=B.idsanpham WHERE sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}}', view);
        promises.push(db.load(sqlCount));
        var sql1 = mustache.render('SELECT * FROM (SELECT * FROM sanpham LEFT JOIN (SELECT bid.gia,nguoimua.namenguoimua,sanpham.idsanpham as id FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}}) AS B ON sanpham.idsanpham=B.id WHERE sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}}) AS T, (SELECT idsanpham AS idsp, DATEDIFF(timeend, CURDATE()) AS datediff FROM sanpham) AS H WHERE T.idsanpham = H.idsp ORDER BY(datediff) limit {{limit}} offset {{offset}}', view);
        promises.push(db.load(sql1));
        var sql2 = mustache.render('SELECT * FROM sanpham LEFT JOIN (SELECT bid.gia,nguoimua.namenguoimua,sanpham.idsanpham as id FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}}) AS B ON sanpham.idsanpham=B.id WHERE sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}} ORDER BY(gia) limit {{limit}} offset {{offset}}', view);
        promises.push(db.load(sql2));
        var sql3 = mustache.render('SELECT * FROM sanpham LEFT JOIN (SELECT bid.gia,nguoimua.namenguoimua,sanpham.idsanpham as id FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.gia FROM (SELECT idsanpham, MAX(gia) AS gia FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.gia = bid_log.gia) AS bid, sanpham, nguoimua WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}}) AS B ON sanpham.idsanpham=B.id WHERE sanpham.danhmuc={{Id2}} AND sanpham.danhmuccon={{Id1}} ORDER BY(gia) DESC limit {{limit}} offset {{offset}}', view);
        promises.push(db.load(sql3));
    }
    Q.all(promises).spread(function(totalRow, cRows, dRows, pRows) {
        var data = {
            total: totalRow[0].total,
            list1: cRows,
            list2: dRows,
            list3: pRows
        }
        deferred.resolve(data);
    });

    return deferred.promise;
}
exports.loadDetails = function(idsanpham) {
    var obj = {
        Id: idsanpham
    };
    var sql = mustache.render(
        'SELECT * FROM sanpham,bid_log,nguoimua WHERE sanpham.idsanpham=bid_log.idsanpham AND bid_log.nguoimua=nguoimua.idnguoimua AND sanpham.idsanpham={{Id}} ORDER BY (bid_log.gia) DESC',
        obj
    );
    return db.load(sql);
}
exports.loadDetailsChild = function(idsanpham) {
    var obj = {
        Id: idsanpham
    };
    var sql = mustache.render(
        'SELECT sanpham.danhmuc,sanpham.danhmuccon,sanpham.idhinh,nguoiban.namenguoiban,nguoiban.diemdanhgia,sanpham.intro,sanpham.receiveinfo,sanpham.warranty,sanpham.timestart,sanpham.timeend,sanpham.`name`, nguoimua.namenguoimua, bid.giahientai, sanpham.Bid,sanpham.price,sanpham.idsanpham FROM (SELECT giaMax.idsanpham, bid_log.nguoimua, giaMax.giahientai FROM (SELECT idsanpham, MAX(gia) AS giahientai FROM bid_log GROUP BY idsanpham) AS giaMax JOIN bid_log ON giaMax.idsanpham = bid_log.idsanpham AND giaMax.giahientai = bid_log.gia) AS bid, sanpham, nguoimua,nguoiban WHERE bid.idsanpham = sanpham.idsanpham AND bid.nguoimua = nguoimua.idnguoimua AND sanpham.nguoiban=nguoiban.idnguoiban AND sanpham.idsanpham={{Id}}',
        obj
    );
    return db.load(sql);
}
exports.loadDetailsNoBid = function(idsanpham) {
    var obj = {
        Id: idsanpham
    };
    var sql = mustache.render(
        'SELECT * FROM sanpham,nguoiban WHERE sanpham.idsanpham={{Id}} AND sanpham.nguoiban=nguoiban.idnguoiban',
        obj
    );
    return db.load(sql);
}
exports.loadDetailsCare = function(idsanpham) {
    var obj = {
        Id: idsanpham
    };
    var sql = mustache.render(
        'SELECT * FROM sanpham,nguoiban WHERE sanpham.nguoiban=nguoiban.idnguoiban AND sanpham.price  >= (SELECT sanpham.price FROM sanpham WHERE sanpham.idsanpham={{Id}}) AND sanpham.idsanpham <>{{Id}} ORDER BY (sanpham.price) ASC limit 3 ' ,
        obj
    );
    return db.load(sql);
}
exports.appreciate = function(entity) {
    var obj = {
        Id: entity
    };
    var sql = mustache.render(
        'SELECT userID, SUM(cong) as Cong,SUM(tru) as Tru FROM chitietdanhgia WHERE userID={{Id}} GROUP BY(userID)  ',
        obj
    );
    return db.load(sql);
}
exports.insert = function(entity) {
    var sql = mustache.render(
        'insert into bid_log(idsanpham,nguoimua,gia,thoigian) values("{{idsanpham}}","{{nguoimua}}","{{Bid}}","{{thoigian}}")',
        entity
    );

    return db.insert(sql);
}
exports.update = function(entity) {
    var sql = mustache.render(
        'update sanpham set Bid = {{BidCount}} where idsanpham = {{idsanpham}}',
        entity
    );

    return db.update(sql);
}
exports.insertFavor = function(entity) {
    var sql = mustache.render(
        'insert into yeuthich(userID,idsanpham) values("{{idnguoimua}}","{{idsanpham}}")',
        entity
    );

    return db.insert(sql);
}
