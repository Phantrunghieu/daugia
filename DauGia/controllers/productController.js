var express=require('express'),
	q = require('q'),
	sanphamRepo=require('../models/sanphamRepo'),
    userController=require('../controllers/userController');
var r = express.Router();
r.get('/byName', function(req, res) {
    if(userController.checklogin===true)
    {
    var name= req.query.name;
    if (!name) {
        res.redirect('/');
    }
    var rec_per_page = 4;
    var curPage = req.query.page ? req.query.page : 1;
    var offset = (curPage - 1) * rec_per_page;
    sanphamRepo.loadPageByName(name, rec_per_page, offset)
        .then(function(data) {
        	
        	
        		var number_of_pages = data.total / rec_per_page;
	            if (data.total % rec_per_page > 0) {
	                number_of_pages++;
	            }
        	
            var pages = [];
            for (var i = 1; i <= number_of_pages; i++) {
                pages.push({
                    pageValue: i,
                    isActive: i === +curPage
                });
            }
            console.log(data.list);
            res.render('product/byName', {
                layoutModels: res.locals.layoutVM,
                productsDate: data.list1,
                productsPriceTang: data.list2,
                productsPriceGiam: data.list3,
                noProducts: data.total === 0,
                CatName: req.query.name,
                lastPage: number_of_pages===curPage,
                pages: pages,
                curPage: curPage,
                prevPage: curPage - 1,
                nextPage: curPage + 1,
                showPrevPage: curPage > 1,
                showNextPage: curPage < number_of_pages - 1,
                checklogin:userController.checklogin,
                name:userController.fullname,
                permissions:userController.permissions
            });
        });
    }
    else
    {
        var name= req.query.name;
    if (!name) {
        res.redirect('/');
    }
    var rec_per_page = 4;
    var curPage = req.query.page ? req.query.page : 1;
    var offset = (curPage - 1) * rec_per_page;
    sanphamRepo.loadPageByName(name, rec_per_page, offset)
        .then(function(data) {
            
            
                var number_of_pages = data.total / rec_per_page;
                if (data.total % rec_per_page > 0) {
                    number_of_pages++;
                }
            
            var pages = [];
            for (var i = 1; i <= number_of_pages; i++) {
                pages.push({
                    pageValue: i,
                    isActive: i === +curPage
                });
            }
            console.log(data.list);
            res.render('product/byName', {
                layoutModels: res.locals.layoutVM,
                productsDate: data.list1,
                productsPriceTang: data.list2,
                productsPriceGiam: data.list3,
                noProducts: data.total === 0,
                CatName: req.query.name,
                lastPage: number_of_pages===curPage,
                pages: pages,
                curPage: curPage,
                prevPage: curPage - 1,
                nextPage: curPage + 1,
                showPrevPage: curPage > 1,
                showNextPage: curPage < number_of_pages - 1,
            });
        });
    }

});
r.get('/details', function(req, res) {
    if(userController.checklogin===true)
    {
    var idsanpham= req.query.id;
    if (!idsanpham) {
        res.redirect('/');
    }
    console.log(userController.userID);
    q.all([
    		sanphamRepo.loadDetails(idsanpham),
    		sanphamRepo.loadDetailsChild(idsanpham),
    		sanphamRepo.loadDetailsNoBid(idsanpham),
            sanphamRepo.loadDetailsCare(idsanpham),
            sanphamRepo.appreciate(userController.userID)
    	])
        .spread(function(pRows,cRows,nRows,mRows,jRows) {
            var vm = {
                layoutVM: res.locals.layoutVM,
                productsDetails: pRows,
                productsDetailsChild: cRows,
                productsDetailsNoBid: nRows,
                productsDetailsCare: mRows,
                noProductsDetailsChild: cRows.length===0,
                noProductsDetailsNoBid: nRows.length===0,
                noProductsDetails: pRows.length === 0,
                checklogin:userController.checklogin,
                name:userController.fullname,
                userID:userController.userID,
                 permissions:userController.permissions,
                appreciate: jRows
            };
            console.log(jRows);
            console.log(mRows);
            res.render('product/details', vm);
            
        });
	}
    else
    {
    var idsanpham= req.query.id;
    if (!idsanpham) {
        res.redirect('/');
    }
    q.all([
            sanphamRepo.loadDetails(idsanpham),
            sanphamRepo.loadDetailsChild(idsanpham),
            sanphamRepo.loadDetailsNoBid(idsanpham),
            sanphamRepo.loadDetailsCare(idsanpham),
        ])
        .spread(function(pRows,cRows,nRows,mRows) {
            var vm = {
                layoutVM: res.locals.layoutVM,
                productsDetails: pRows,
                productsDetailsChild: cRows,
                productsDetailsNoBid: nRows,
                productsDetailsCare: mRows,
                noProductsDetailsChild: cRows.length===0,
                noProductsDetailsNoBid: nRows.length===0,
                noProductsDetails: pRows.length === 0,
            };
            res.render('product/details', vm);
            
        });
    }
});
r.post('/detailss', function(req, res) {
    if(userController.checklogin===true)
    {
    var idsanpham= req.body.id;
    if (!idsanpham) {
        res.redirect('/');
    }
    console.log(userController.userID);
    q.all([
    		sanphamRepo.loadDetails(idsanpham),
    		sanphamRepo.loadDetailsChild(idsanpham),
    		sanphamRepo.loadDetailsNoBid(idsanpham),
            sanphamRepo.loadDetailsCare(idsanpham),
            sanphamRepo.appreciate(userController.userID)
    	])
        .spread(function(pRows,cRows,nRows,mRows,jRows) {
            var vm = {
                layoutVM: res.locals.layoutVM,
                productsDetails: pRows,
                productsDetailsChild: cRows,
                productsDetailsNoBid: nRows,
                productsDetailsCare: mRows,
                noProductsDetailsChild: cRows.length===0,
                noProductsDetailsNoBid: nRows.length===0,
                noProductsDetails: pRows.length === 0,
                checklogin:userController.checklogin,
                name:userController.fullname,
                userID:userController.userID,
                 permissions:userController.permissions,
                appreciate: jRows
            };
            console.log(jRows);
            console.log(mRows);
            res.render('product/details', vm);
            
        });
	}
    else
    {
    var idsanpham= req.query.id;
    if (!idsanpham) {
        res.redirect('/');
    }
    q.all([
            sanphamRepo.loadDetails(idsanpham),
            sanphamRepo.loadDetailsChild(idsanpham),
            sanphamRepo.loadDetailsNoBid(idsanpham),
            sanphamRepo.loadDetailsCare(idsanpham),
        ])
        .spread(function(pRows,cRows,nRows,mRows) {
            var vm = {
                layoutVM: res.locals.layoutVM,
                productsDetails: pRows,
                productsDetailsChild: cRows,
                productsDetailsNoBid: nRows,
                productsDetailsCare: mRows,
                noProductsDetailsChild: cRows.length===0,
                noProductsDetailsNoBid: nRows.length===0,
                noProductsDetails: pRows.length === 0,
            };
            res.render('product/details', vm);
            
        });
    }
});


r.get('/byCat', function(req, res) {
    if(userController.checklogin===true)
    {
    var id1= req.query.id1;//danhmuccon
    var id2=req.query.id2;//danhmuc
    console.log(id1);
    
    if (id2==null) {
        res.redirect("/");
    }
    console.log(id2);
    var rec_per_page = 4;
    var curPage = req.query.page ? req.query.page : 1;
    var offset = (curPage - 1) * rec_per_page;
    sanphamRepo.loadPageByCatID(id1,id2, rec_per_page, offset)
        .then(function(data) {
        	
        	
        		var number_of_pages = data.total / rec_per_page;
	            if (data.total % rec_per_page > 0) {
	                number_of_pages++;
	            }
        	
            var pages = [];
            for (var i = 1; i <= number_of_pages; i++) {
                pages.push({
                    pageValue: i,
                    isActive: i === +curPage
                });
            }
            res.render('product/byCat', {
                layoutModels: res.locals.layoutVM,
                productsDate: data.list1,
                productsPriceTang: data.list2,
                productsPriceGiam: data.list3,
                noProducts: data.total === 0,
                CatId1: req.query.id1,//danhmuccon
                CatId2: req.query.id2,//danhmuc
                lastPage: number_of_pages===curPage,
                pages: pages,
                curPage: curPage,
                prevPage: curPage - 1,
                nextPage: curPage + 1,
                showPrevPage: curPage > 1,
                showNextPage: curPage < number_of_pages - 1,
                checklogin:userController.checklogin,
                name:userController.fullname,
                 permissions:userController.permissions

            });
        });
    }
    else
    {
        var id1= req.query.id1;//danhmuccon
    var id2=req.query.id2;//danhmuc
    console.log(id1);
    
    if (id2==null) {
        res.redirect("/");
    }
    console.log(id2);
    var rec_per_page = 4;
    var curPage = req.query.page ? req.query.page : 1;
    var offset = (curPage - 1) * rec_per_page;
    sanphamRepo.loadPageByCatID(id1,id2, rec_per_page, offset)
        .then(function(data) {
            
            
                var number_of_pages = data.total / rec_per_page;
                if (data.total % rec_per_page > 0) {
                    number_of_pages++;
                }
            
            var pages = [];
            for (var i = 1; i <= number_of_pages; i++) {
                pages.push({
                    pageValue: i,
                    isActive: i === +curPage
                });
            }
            res.render('product/byCat', {
                layoutModels: res.locals.layoutVM,
                productsDate: data.list1,
                productsPriceTang: data.list2,
                productsPriceGiam: data.list3,
                noProducts: data.total === 0,
                CatId1: req.query.id1,//danhmuccon
                CatId2: req.query.id2,//danhmuc
                lastPage: number_of_pages===curPage,
                pages: pages,
                curPage: curPage,
                prevPage: curPage - 1,
                nextPage: curPage + 1,
                showPrevPage: curPage > 1,
                showNextPage: curPage < number_of_pages - 1,
            });
        });
    }	
});
r.post('/details', function(req, res) {
    q.all([
            sanphamRepo.insert(req.body),
            sanphamRepo.update(req.body),
        ])
    .spread(function(data,rowChange) {
        res.redirect('/');
    }).catch(function(err) {
        console.log(err);
        res.end('insert fail');
    });
});
r.post('/details/Favor', function(req, res) {
    q.all([
            sanphamRepo.insertFavor(req.body)
        ])
    .spread(function(data,rowChange) {
        res.redirect('/');
    }).catch(function(err) {
        console.log(err);
        res.end('insert fail');
    });
});
module.exports = r;