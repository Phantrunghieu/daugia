var express=require('express');
var q = require('q');
var homeRepo=require('../models/homeRepo');
var userController=require('../controllers/userController');
var r = express.Router();
r.get('/', function(req, res) {
	if(userController.checklogin===true)
{
		q.all([
    	homeRepo.loadTop5Price(),
    	homeRepo.loadTop5Bid(),
    	homeRepo.loadTop5Date()
    ]).spread(function(cRows, dRows, pRows) {
    	var vm={
    		layoutVM: res.locals.layoutVM,
    		curUser: req.session.user,
    		productsPrice: cRows,
			productsBid: dRows,
			productsDate: pRows,
    
	
		checklogin:userController.checklogin,
		name:userController.fullname,
		 permissions:userController.permissions
    	
    };

    res.render('home/index', vm);
});
}
else{
	q.all([
    	homeRepo.loadTop5Price(),
    	homeRepo.loadTop5Bid(),
    	homeRepo.loadTop5Date()
    ]).spread(function(cRows, dRows, pRows) {
    	var vm={
    		layoutVM: res.locals.layoutVM,
    		curUser: req.session.user,
    		productsPrice: cRows,
			productsBid: dRows,
			productsDate: pRows,
	
		
    	layoutVM: res.locals.layoutVM
    };

    res.render('home/index', vm);
	});
}	

	
});
r.get('/gioithieu',function(req,res){
	if(userController.checklogin===true)
{
	var vm={
		checklogin:userController.checklogin,
		name:userController.fullname,
		 permissions:userController.permissions,
		layoutVM: res.locals.layoutVM
	};
	res.render('home/gioithieu',vm);
}else{
var vm={
	
		layoutVM: res.locals.layoutVM
	};
	res.render('home/gioithieu',vm);
}
});
r.get('/gopy',function(req,res){
	if(userController.checklogin===true)
{
	var vm={
		checklogin:userController.checklogin,
		name:userController.fullname,
		 permissions:userController.permissions,
		layoutVM: res.locals.layoutVM
	};
	res.render('home/gopy',vm);
}else{
	var vm={
	
		layoutVM: res.locals.layoutVM
	};
	res.render('home/gopy',vm);
}
});
r.get('/dieukhoan',function(req,res){
	if(userController.checklogin===true)
{
	var vm={
		checklogin:userController.checklogin,
		name:userController.fullname,
		 permissions:userController.permissions,
		layoutVM: res.locals.layoutVM
	};
	res.render('home/dieukhoan',vm);
}
else{
	var vm={
		
		layoutVM: res.locals.layoutVM
	};
	res.render('home/dieukhoan',vm);
}
});
r.get('/trogiup',function(req,res){
	if(userController.checklogin===true)
{
	var vm={
		checklogin:userController.checklogin,
		name:userController.fullname,
		permissions:userController.permissions,
		layoutVM: res.locals.layoutVM
	};
	res.render('home/trogiup',vm);
}
else{
	var vm={
		
		layoutVM: res.locals.layoutVM
	};
	res.render('home/trogiup',vm);
}
});


module.exports=r;