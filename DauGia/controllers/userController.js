var express = require('express');
var crypto = require('crypto');
var moment = require('moment');
var taikhoanRepo = require('../models/taikhoanRepo');
var restrict = require('../middle-wares/restrict');
var danhmucRepo = require('../models/danhmucRepo');
var nodemailer = require("nodemailer");
var q = require('q');

var r = express.Router();
var nodemailer = require("nodemailer");
var smtpTransport = nodemailer.createTransport({host: 'smtp.gmail.com',
    port: 465,
    secure: true, // secure:true for port 465, secure:false for port 587
    auth: {
        user: 'higata14ctt@gmail.com',
        pass: 'hieugamtai'
    },
    tls: {
        secureProtocol: "TLSv1_method"
    }
});




////////////////////////////////////////get login
r.get('/login', function (req, res) {
    if (req.session.isLogged === true) {
        res.redirect('/');
    } else {
        var vm = {
            layoutModels: res.locals.layoutVM,
            checkerr: false,
            err: ''

        };
        res.render('user/login', vm);
    }
});
///////////////////////////////gửi login
r.post('/login', function (req, res) {

    var encrypt = crypto.createHash('md5').update(req.body.password).digest('hex');

    var remember = req.body.remember ? true : false;

    taikhoanRepo.loaduser(req.body.email, encrypt)
        .then(function (user) {

            if (user.length === 0) {
                var checklogin = false;
                var vm = {
                    layoutModels: res.locals.layoutVM,
                    checkerr: true,

                    err: 'Sai tên đăng nhập hoặc mật khẩu'
                };
                res.render('user/login', vm);
            } else {
                req.session.isLogged = true;
                req.session.user = user;
                module.exports.checklogin = req.session.isLogged;
                module.exports.userID = user[0].userID; //////#gam cái này return userID (string)
                module.exports.fullname = user[0].name;
                 module.exports.permissions = user[0].permission;
                if (remember === true) {
                    var hour = 1000 * 60 * 60 * 24;
                    req.session.cookie.expires = new Date(Date.now() + hour);
                    req.session.cookie.maxAge = hour;
                }
                if (user[0].permission === 1) {

                    res.redirect('/profile');

                } else


                    res.redirect('/');
            }

        });
});


////////////////////////////log out
r.get('/logout', function (req, res) {
    req.session.isLogged = false;
    req.session.user = null;

    module.exports.checklogin = false;
    req.session.cookie.expires = new Date(Date.now() - 1000);
    res.redirect('/');
});
///////////////////////get đăng ký
r.get('/register', function (req, res) {
    taikhoanRepo.loadallusersemail()
        .then(function (user) {
            console.log(user);
            req.session.user = user;
            var vm = {
                layoutModels: res.locals.layoutVM,
                checkerr: false,
                err: ''
            };
            res.render('user/register', vm);
        });
});
///////////////////////////////////gửi đăng ký
r.post('/register', function (req, res) {

    var encrypt = crypto.createHash('md5').update(req.body.password).digest('hex');
    var DOB = moment(req.body.DOB, 'D/M/YYYY').format('YYYY-MM-DDTHH:mm');


    var email = req.body.email;
    var password = encrypt;
    var name = req.body.name;
    var adress = req.body.adress;
    var dob = DOB;
    var permission = 0;
    for (i = 0; i < req.session.user.length; i++) {
        if (email === req.session.user[i].email) {
            var checkemail = true;
            var vm = {
                layoutVM: res.locals.layoutVM,
                checkerr: true,
                err: 'email đã tồn tại!'

            };
            res.render('user/register', vm);

        } else checkemail = false;
    }
    if (checkemail === false) {
        taikhoanRepo.insertuser(email, password, name, adress, dob, permission)
            .then(function (email) {
                console.log("đã đăng ký");
                var vm = {
                    layoutVM: res.locals.layoutVM,
                    checkerr: true,
                    err: 'Đăng ký thành công.'

                };
                res.render('user/register', vm);

            });
    }
});




////////////////////get profile
r.get('/profile', function (req, res) {
    if (req.session.isLogged === true) {
        if (req.session.user[0].permission === 1) {
            var vm = {
                layoutVM: res.locals.layoutVM,
                checklogin: true,
                name: req.session.user[0].name,
                userID: req.session.user[0].userID,
                 permissions: req.session.user[0].permission,
                email: req.session.user[0].email,
                password: req.session.user[0].password,
                adress: req.session.user[0].adress,
                DOB: req.session.user[0].DOB,
                timestamp: req.session.user[0].timestamp,
                permission: "Admin"

            };
            res.render('user/profile', vm);
        }
        if (req.session.user[0].permission === 0) {
            var vm = {
                layoutVM: res.locals.layoutVM,
                checklogin: true,
                userID: req.session.user[0].userID,
                name: req.session.user[0].name,
                email: req.session.user[0].email,
                password: req.session.user[0].password,
                adress: req.session.user[0].adress,
                DOB: req.session.user[0].DOB,
                timestamp: req.session.user[0].timestamp,
 permissions: req.session.user[0].permission,
                permission: "Người Mua"

            };
            res.render('user/profile', vm);
        }
        if (req.session.user[0].permission === 2) {
            var vm = {
                layoutVM: res.locals.layoutVM,
                checklogin: true,
                userID: req.session.user[0].userID,
                name: req.session.user[0].name,
                email: req.session.user[0].email,
                password: req.session.user[0].password,
                adress: req.session.user[0].adress,
                DOB: req.session.user[0].DOB,
                timestamp: req.session.user[0].timestamp,
                 permissions: req.session.user[0].permission,
                permission: "Người bán"

            };
            res.render('user/profile', vm);
        }

    } else res.redirect('/');
});
/////////////////////////get userlist
r.get('/accountmanager', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 1) {
        taikhoanRepo.loadallusers()
            .then(function (user) {
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutVM: res.locals.layoutVM,
                    user: user

                };
                res.render('user/accountmanager', vm);
            });
    } else res.redirect('/');

});
r.post('/accountmanager', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 1) {
        taikhoanRepo.deleteuser(req.body.userID)
            .then(function (user) {
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutVM: res.locals.layoutVM,
                    checkerr: true,
                    err: "Đã xóa"

                };
                res.render('user/accountmanager', vm);
            });
    } else res.redirect('/');

});




////////////////////////edit profile
r.get('/editprofile', function (req, res) {
    if (req.session.isLogged === true) {
        var vm = {
            checklogin: true,
            name: req.session.user[0].name,
             permissions: req.session.user[0].permission,
            layoutModels: res.locals.layoutVM,
            checkerr: false,
            err: ''
        };
        res.render('user/editprofile', vm);
    } else res.redirect('/');
});
r.post('/editprofile', function (req, res) {
    if (req.session.isLogged === true) {
        console.log(req.body.DOB);
        taikhoanRepo.edituser(req.session.user[0].userID, req.body.email, req.body.name, req.body.adress, req.body.DOB)
            .then(function (user) {
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutVM: res.locals.layoutVM,
                    checkerr: true,
                    err: 'Sửa đổi thông tin thành công'
                };
                res.render('user/editprofile', vm);
            });
    } else res.redirect('/');
});
////////////////////////edit account
r.get('/editaccount', function (req, res) {
    var vm = {
        layoutModels: res.locals.layoutVM,
        checkerr: false,
        err: ''
    };
    res.render('user/editaccount', vm);
});
r.post('/editaccount', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 1) {
        taikhoanRepo.editaccount(req.session.user[0].userID, req.body.email, req.body.password, req.body.adress, req.body.DOB)
            .then(function (user) {
                var vm = {
                    layoutVM: res.locals.layoutVM,
                    checkerr: true,
                    err: 'Sửa đổi thông tin thành công'

                };
                res.render('user/editaccount', vm);
            });
    } else res.redirect('/');

});
//////////////////////////////////////////change password
r.get('/changepassword', function (req, res) {
    if (req.session.isLogged === true) {
        var vm = {
            checklogin: true,
            name: req.session.user[0].name,
             permissions: req.session.user[0].permission,
            layoutModels: res.locals.layoutVM,

            checkerr: false,
            err: ''
        };
        res.render('user/doimatkhau', vm);
    } else res.redirect('/');
});
r.post('/changepassword', function (req, res) {
    if (req.session.isLogged === true) {
        encrypt = crypto.createHash('md5').update(req.body.password).digest('hex');
        encrypt2 = crypto.createHash('md5').update(req.body.newpassword).digest('hex');
        if (encrypt === req.session.user[0].password) {
            taikhoanRepo.changepassword(req.session.user[0].userID, encrypt2)
                .then(function (user) {
                    
                    var vm = {
                        checklogin: true,
                        name: req.session.user[0].name,
                         permissions: req.session.user[0].permission,
                        layoutVM: res.locals.layoutVM,
                        checkerr: true,
                        err: 'Đổi password thành công'

                    };
                    res.render('user/doimatkhau', vm);
                });
        } else {
            var vm = {
                checklogin: true,
                name: req.session.user[0].name,
                 permissions: req.session.user[0].permission,
                layoutVM: res.locals.layoutVM,
                checkerr: true,
                err: 'Mật khẩu không đúng'

            };
            res.render('user/doimatkhau', vm);
        }
    } else res.redirect('/');
});
///////////////////////////đổi thành người bán


///////////////////////////xóa user.
r.get('/deleteaccount', function (req, res) {
    var vm = {
        checklogin: true,
        name: req.session.user[0].name,
         permissions: req.session.user[0].permission,
        layoutModels: res.locals.layoutVM,
        checkerr: false,
        err: ''
    };
    res.render('user/deleteaccount', vm);
});
r.post('/deleteaccount', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 1) {
        taikhoanRepo.deleteuser(req.session.user[0].userID)
            .then(function (user) {
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutVM: res.locals.layoutVM,
                    checkerr: true,
                    err: 'đã xóa tài khoản'

                };
                res.render('user/deleteaccount', vm);
            });
    } else res.redirect('/');
});
////////////////////////////////////////////
r.get('/loaddoiban', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 1) {
        taikhoanRepo.loaddoiban()
            .then(function (user) {
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutModels: res.locals.layoutVM,
                    user: user,
                    checkerr: false,
                    err: ''
                };
                res.render('user/danhsachdoiban', vm);
            });
    } else res.redirect('/');
});
r.post('/loaddoiban', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 1) {
        taikhoanRepo.changepermission(req.body.accept)
            .then(function (user) {

                smtpTransport.sendMail({ 
   from: " No-Reply@gmail.com",
   to: user[0].email,
   subject: "Thông báo về việc reset mật khẩu",
   text: "Bạn đã được cho phép bán hàng" // body
}, function(error, response){  //callback
   if(error){
       console.log(error);
   }else{
       console.log("Message sent: " + response.message);
   }
   
   smtpTransport.close(); 
});
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutVM: res.locals.layoutVM,
                    checkerr: true,
                    err: 'đã cho phép bán'

                };
                res.render('user/danhsachdoiban', vm);
            });


    } else res.redirect('/');
});
r.post('/xoadoiban', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 1) {
        taikhoanRepo.xoadoiban(req.body.userIDx)
            .then(function (user) {
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutVM: res.locals.layoutVM,
                    checkerr: true,
                    err: 'đã xóa'

                };
                res.render('user/danhsachdoiban', vm);
            });


    } else res.redirect('/');
});
///////////////////////////////////////////xin bán
r.get('/xinban', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission !== 1) {

        var vm = {
            checklogin: true,
            name: req.session.user[0].name,
             permissions: req.session.user[0].permission,
            layoutModels: res.locals.layoutVM,

        };
        res.render('user/xinban', vm);

    } else res.redirect('/');
});
r.post('/xinban', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission !== 1) {
        taikhoanRepo.xinban(req.session.user[0].userID)
            .then(function (user) {
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutVM: res.locals.layoutVM,
                    checkerr: true,
                    err: 'đã gửi yêu cầu'

                };
                res.render('user/xinban', vm);
            });
    } else res.redirect('/');
});


////////////////////////danh sách yêu thích
r.get('/yeuthich', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission !== 1) {
        taikhoanRepo.danhsachyeuthich(req.session.user[0].userID)
            .then(function (product) {
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutModels: res.locals.layoutVM,
                    product: product
                };
                res.render('user/danhsachyeuthich', vm);
            });
    } else res.redirect('/');

});
// r.post('/yeuthich',   function (req, res) {
//     if( req.session.isLogged === true&& req.session.user[0].permission!==1)
//     {
//  taikhoanRepo.xinban(req.session.user[0].userID)
//         .then(function (user) {
//     var vm = {
//         layoutVM: res.locals.layoutVM,
//         checkerr:true,
//         err:'đã gửi yêu cầu'

//     };
//     res.render('user/danhsachyeuthich', vm);
//     });
// }
// else res.redirect('/');
// });


//////////////////////////////////////load danh sách đánh giá
r.get('/danhsachdanhgia', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 0 || req.session.user[0].permission === 2) {
        taikhoanRepo.dsdanhgia(req.session.user[0].userID)
            .then(function (review) {

                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutModels: res.locals.layoutVM,
                    review: review
                };

                res.render('user/danhsachdanhgia', vm);
            });
    } else res.redirect('/');

});


/////////////////////////////////////load danh sách danh mục
r.get('/danhsachdanhmuc', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 1) {
        taikhoanRepo.dsdanhmuc()
            .then(function (danhmuc) {
                console.log(danhmuc);
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutModels: res.locals.layoutVM,
                    danhmuc: danhmuc
                };

                res.render('user/danhsachdanhmuc', vm);
            });
    } else res.redirect('/');

});
r.post('/danhsachdanhmuc', function (req, res) {

    taikhoanRepo.dsdanhmuccon(req.body.id)
        .then(function (danhmuccon) {
            console.log(danhmuccon);
            var vm = {
                checklogin: true,
                name: req.session.user[0].name,
                 permissions: req.session.user[0].permission,
                layoutModels: res.locals.layoutVM,
                danhmuccon: danhmuccon,
                danhmuc: danhmuccon[0].danhmuc
            };

            res.render('user/danhsachdanhmuccon', vm);
        });

});


r.post('/danhsachsanpham', function (req, res) {

    taikhoanRepo.dsasanpham(req.body.idsp)
        .then(function (sanpham) {
            console.log(sanpham);
            var vm = {
                checklogin: true,
                name: req.session.user[0].name,
                 permissions: req.session.user[0].permission,
                layoutModels: res.locals.layoutVM,
                sanpham: sanpham
            };
           
            res.render('user/danhsachsanpham', vm);
        });

});
r.post('/xoadanhmuc', function (req, res) {

    taikhoanRepo.xoadanhmuc(req.body.idx)
        .then(function (danhmuccon) {

            var vm = {
                checklogin: true,
                name: req.session.user[0].name,
                 permissions: req.session.user[0].permission,
                layoutModels: res.locals.layoutVM,
                checkerr: true,
                err: "xóa danh mục thành công"
            };

            res.render('user/danhsachdanhmuc', vm);
        });

});
r.post('/xoadanhmuccon', function (req, res) {

    taikhoanRepo.xoadanhmuccon(req.body.idx2)
        .then(function (danhmuccon) {

            var vm = {
                checklogin: true,
                name: req.session.user[0].name,
                 permissions: req.session.user[0].permission,
                layoutModels: res.locals.layoutVM,
                checkerr: true,
                err: "xóa danh mục thành công"
            };

            res.render('user/danhsachdanhmuccon', vm);
        });

});
r.post('/xoasanpham', function (req, res) {

    taikhoanRepo.xoasanpham(req.body.id)
        .then(function (sp) {

            var vm = {
                checklogin: true,
                name: req.session.user[0].name,
                 permissions: req.session.user[0].permission,
                layoutModels: res.locals.layoutVM,
                checkerr: true,
                err: "xóa sản phẩm thành công"
            };

            res.render('user/danhsachdanhmuccon', vm);
        });

});

r.post('/suadanhmuc', function (req, res) {

    taikhoanRepo.suadanhmuc(req.body.ids, req.body.name)
        .then(function (sp) {

            var vm = {
                checklogin: true,
                name: req.session.user[0].name,
                 permissions: req.session.user[0].permission,
                layoutModels: res.locals.layoutVM,
                checkerr: true,
                err: "sửa danh mục thành công"
            };

            res.render('user/danhsachdanhmuc', vm);
        });

});
r.post('/suadanhmuccon', function (req, res) {

    taikhoanRepo.suadanhmuccon(req.body.ids, req.body.name)
        .then(function (sp) {

            var vm = {
                checklogin: true,
                name: req.session.user[0].name,
                 permissions: req.session.user[0].permission,
                layoutModels: res.locals.layoutVM,
                checkerr: true,
                err: "sửa danh mục thành công"
            };

            res.render('user/danhsachdanhmuccon', vm);
        });

});

r.post('/themdanhmuccon', function (req, res) {

    taikhoanRepo.themdanhmuccon(req.body.idt, req.body.namet)
        .then(function (sp) {

            var vm = {
                checklogin: true,
                name: req.session.user[0].name,
                 permissions: req.session.user[0].permission,
                layoutModels: res.locals.layoutVM,
                checkerr: true,
                err: "thêm danh mục thành công"
            };

            res.render('user/danhsachdanhmuccon', vm);
        });

});
r.post('/themdanhmuc', function (req, res) {

    taikhoanRepo.themdanhmuc(req.body.idt, req.body.namet)
        .then(function (sp) {

            var vm = {
                checklogin: true,
                name: req.session.user[0].name,
                 permissions: req.session.user[0].permission,
                layoutModels: res.locals.layoutVM,
                checkerr: true,
                err: "thêm danh mục thành công"
            };

            res.render('user/danhsachdanhmuccon', vm);
        });

});
/////////////////////////danh sách danh mục con
// r.get('/danhsachdanhmuccon', function (req, res) {
//     if (req.session.isLogged === true && req.session.user[0].permission === 1) {

//     }

//     else res.redirect('/');

// });


/////////////////////////////////quên mật khẩu
r.get('/quenmatkhau', function (req, res) {
    if (req.session.isLogged !== true) {

        var vm = {
            layoutModels: res.locals.layoutVM,
            checkerr: false,
            err: ""
        };

        res.render('user/quenmatkhau', vm);

    } else res.redirect('/');

});
r.post('/quenmatkhau', function (req, res) {
    if (req.session.isLogged !== true) {
        taikhoanRepo.quenmatkhau(req.body.email)
            .then(function (temp) {

                var vm = {
                    layoutModels: res.locals.layoutVM,
                    checkerr: true,
                    err: "Yêu cầu đã được gửi"
                };

                res.render('user/quenmatkhau', vm);
            });
    } else res.redirect('/');

});
///////////////////////load danh sach quen mat khau
r.get('/danhsachquenmatkhau', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 1) {
        taikhoanRepo.dsquenmatkhau()
            .then(function (ds) {

                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    ds: ds,
                    layoutModels: res.locals.layoutVM,
                    checkerr: false,
                    err: ""
                };

                res.render('user/danhsachquenmatkhau', vm);
            });
    } else res.redirect('/');

});
r.post('/danhsachquenmatkhau', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 1) {
        taikhoanRepo.resetmatkhau(req.body.email)
            .then(function (ds) {
smtpTransport.sendMail({ 
   from: " No-Reply@gmail.com",
   to: req.body.email,
   subject: "Thông báo về việc reset mật khẩu",
   text: "Mật khẩu của bạn sẽ được đổi thành: thefactthat" // body
}, function(error, response){  //callback
   if(error){
       console.log(error);
   }else{
       console.log("Message sent: " + response.message);
   }
   
   smtpTransport.close(); 
});
                var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutModels: res.locals.layoutVM,
                    checkerr: true,
                    err: "đã reset mật khẩu thành thefactthat"
                };

                res.render('user/danhsachquenmatkhau', vm);
            });

    } else res.redirect('/');

});

///////////////////////////////xem danh sách sản phẩm đang đấu giá
r.get('/danhsachdangdaugia', function (req, res) {
 if (req.session.isLogged === true && req.session.user[0].permission === 0||req.session.user[0].permission === 2) {
      taikhoanRepo.loadsanphamdangmua(req.session.user[0].userID)
.then(function(sanpham){
    console.log(req.session.user[0].userID);
    console.log(sanpham);
 var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutModels: res.locals.layoutVM,
                    sanpham:sanpham,
                    checkerr: false,
                    err: ""
                };

                res.render('user/sanphamdangmua', vm);
            });
 }
 else res.redirect('/');

});

r.get('/danhsachdangban', function (req, res) {
 if (req.session.isLogged === true &&req.session.user[0].permission === 2) {
      taikhoanRepo.loadsanphamdangban(req.session.user[0].userID)
.then(function(sanpham){
    console.log(req.session.user[0].userID);
    console.log(sanpham);
 var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutModels: res.locals.layoutVM,
                    sanpham:sanpham,
                    checkerr: false,
                    err: ""
                };

                res.render('user/sanphamdangban', vm);
            });
 }
 else res.redirect('/');

});
//////////////////////xem danh sách các sản phẩm dã mua
r.get('/danhsachdaban', function (req, res) {
   if (req.session.isLogged === true &&req.session.user[0].permission === 2) {
     taikhoanRepo.loadsanphamdaban(req.session.user[0].userID)
.then(function(sanpham){
console.log(sanpham);
var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutModels: res.locals.layoutVM,
                    sanpham:sanpham,
                    checkerr: false,
                    err: ""
                };

                res.render('user/sanphamdaban', vm);
            });
 }
 else res.redirect('/');

});

r.get('/danhsachdamua', function (req, res) {
   if (req.session.isLogged === true &&req.session.user[0].permission === 2) {
     taikhoanRepo.loadsanphamdaban(req.session.user[0].userID)
.then(function(sanpham){
console.log(sanpham);
var vm = {
                    checklogin: true,
                    name: req.session.user[0].name,
                     permissions: req.session.user[0].permission,
                    layoutModels: res.locals.layoutVM,
                    sanpham:sanpham,
                    checkerr: false,
                    err: ""
                };

                res.render('user/sanphamdamua', vm);
            });
 }
 else res.redirect('/');

});

r.get('/dangban', function (req, res) {
    if (req.session.isLogged === true && req.session.user[0].permission === 2)
    {
        q.all([
            danhmucRepo.loadAllDm(),
            danhmucRepo.loadAllDmc()
        ]).spread(function(cRows, dRows) {
            var vm={
                layoutVM: res.locals.layoutVM,
                danhmuc: cRows,
                danhmuccon: dRows,
                checklogin: true,
                userID: req.session.user[0].userID
            };

            res.render('user/dangban', vm);
        });
    }
    else
    {
        res.redirect('/');
    }
  
});
r.post('/dangban',function(req,res)
{
    console.log(req.body);
    q.all([
            taikhoanRepo.insertsp(req.body)
        ])
    
    .spread(function(data)
    {
         res.redirect('/');
     }).catch(function(err)
     {
         console.log(err);
         res.end("insert fail");
     });
});

module.exports = r;