var express = require('express'),
    handlebars = require('express-handlebars'),
    morgan = require('morgan'),
    handlebars_sections = require('express-handlebars-sections'),
    path = require('path'),
    bodyParser = require('body-parser'),
    moment = require('moment'),
    wnumb = require('wnumb');
var session = require('express-session');
var app = express();
var homeController = require('./controllers/homeController');
var userController = require('./controllers/userController');
var productController = require('./controllers/productController');
var handle404 = require('./middle-wares/handle-404');
var handleLayout = require('./middle-wares/handleLayout');
var app = express();
var hbs = require('handlebars');
hbs.registerHelper('ifCond', function (v1, v2, options) {
    if (v1 === v2) {
        return options.fn(this);
    } else {
        return options.inverse(this);
    }
});


app.use(morgan('dev'));
app.engine('hbs', handlebars({
    extname: 'hbs',
    defaultLayout: 'main',
    layoutsDir: 'views/_layouts/',
    partialsDir: 'views/_partials/',
    helpers: {
        section: handlebars_sections(),
        now: function () {
            return moment().format('DD/MM/YYYY - HH:mm:ss');
        },
        date_format: function (n) {
            return moment(n).format('DD/MM/YYYY - HH:mm:ss');
        },
		sub_date: function(date)
        {
            return date-new Date();
        },
        number_format: function (n) {
            var nf = wnumb({
                thousand: ','
            });
            return nf.to(n);
        }
    }

}));

app.set('view engine', 'hbs');


app.use(express.static(
    path.resolve(__dirname, 'public')
));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({
    extended: false
}));
app.use(session({ secret: 'higatahieugamtai', cookie: { maxAge: 60000 }, resave: true, saveUninitialized: true }))

app.use(handleLayout);
app.use('/', homeController);
app.use('/product', productController);
app.use('/', userController);
app.use(handle404);



app.listen(3000);


module.exports = app;

